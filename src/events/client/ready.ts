import { Client } from "discord.js";

import {
  dbReminder,
  deleteReminder,
  infoReminder,
  OptionReminder,
  sendReminder,
  setTimeoutReminder,
  updateReminder,
} from "../../utils/commands/reminder";
import { readSQL } from "../../utils/db";
import { logStart } from "../../utils/misc";

export const once = true;

/** https://discord.js.org/#/docs/discord.js/main/class/Client?scrollTo=e-ready */
export default async (client: Client) => {
  console.log(logStart("Connection", true));

  // Restart all the timeout about reminders here
  new Promise((ok, ko) => {
    // Fetch all reminders
    client.db.all(readSQL("reminder/select"), [], (err, row) => {
      if (err) {
        ko(err);
      }

      // Send all the current reminders
      ok(row);
    });
  })
    .then((data) => {
      const now = Date.now();
      (data as dbReminder[]).forEach((element) => {
        const info = {
          locale: element.locale,
          message: element.data,
          createdAt: Number(element.creation_date),
          channelId: `${element.channel_id}`,
          userId: `${element.user_id}`,
          guildId: `${element.guild_id}`,
        } as infoReminder;

        if (element.expiration_date <= now) {
          sendReminder(client, info, element.option_id as OptionReminder)
            .then(() =>
              // Reminder expired
              deleteReminder(client, element.creation_date, `${element.user_id}`).then((res) => {
                if (res != true) {
                  throw res;
                }
              }),
            )
            .catch((err) => {
              throw err;
            });
        } else {
          // Restart timeout
          const timeoutId = setTimeoutReminder(
            client,
            info,
            element.option_id,
            (element.expiration_date - now) / 1000,
          );

          // Update timeout in database
          element.timeout_id = String(timeoutId);
          updateReminder(client, element).then((res) => {
            if (res != true) {
              throw res;
            }
          });
        }
      });
    })
    .catch((err) => {
      throw err;
    });
};
