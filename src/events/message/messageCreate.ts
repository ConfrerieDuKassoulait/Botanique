import { Client, EmbedBuilder, Message, TextBasedChannel } from "discord.js";

import { handleAttachments } from "../../utils/events/citation";
import { getLocale } from "../../utils/locales";
import { userWithNickname } from "../../utils/misc";
import { RegexC, RegExpFlags } from "../../utils/regex";
import { showDate } from "../../utils/time";

/** https://discord.js.org/#/docs/discord.js/main/class/Client?scrollTo=e-messageCreate */
export default async (message: Message, client: Client) => {
  // Ignore message if
  if (
    // Author is a bot
    message.author.bot ||
    // Author is Discord
    message.author.system ||
    // Message isn't a message
    message.system ||
    // Message is in PM (future-proof if we add Intents.FLAGS.DIRECT_MESSAGES)
    !message.guild ||
    // Guild is offline
    !message.guild.available
  ) {
    return;
  }

  /* Citation */
  const regex =
    /https:\/\/(?:canary\.|ptb\.)?discord(?:app)?\.com\/channels\/(\d{17,19})\/(\d{17,19})\/(\d{17,19})/g;
  const urls = message.content.match(RegexC(regex, RegExpFlags.Global));

  // Ignore message if there is no URLs
  if (!urls) {
    return;
  }

  const messages = (
    await Promise.all(
      urls
        .reduce(
          (
            data: {
              message_id: string;
              channel: TextBasedChannel;
            }[] = [],
            match,
          ) => {
            const [, guild_id, channel_id, message_id] = RegexC(regex).exec(
              match,
            ) as RegExpExecArray;

            // If message posted in another guild
            if (guild_id !== message.guild?.id) {
              return data;
            }

            const channel = message.guild.channels.cache.get(channel_id);

            // If channel doesn't exist in the guild and isn't text
            if (!channel || !channel.isTextBased()) {
              return data;
            }

            data.push({ message_id, channel });

            return data;
          },
          [],
        )
        .map(async ({ message_id, channel }) => {
          let quoted_message = await channel.messages.fetch(message_id).catch(() => undefined);

          // If it's a reference, we only check for reference once
          const message_reference = quoted_message?.reference;
          if (message_reference && message_reference.messageId) {
            const channel_reference = client.channels.cache.get(message_reference.channelId);
            if (!channel_reference?.isTextBased()) {
              return;
            }

            quoted_message = await channel_reference.messages
              .fetch(message_reference.messageId)
              .catch(() => undefined);
          }

          // If message doesn't exist or empty
          if (
            !quoted_message ||
            (!quoted_message.content && quoted_message.attachments.size === 0)
          ) {
            return;
          }

          return quoted_message;
        }),
    )
  )
    // Remove undefined elements
    .filter(Boolean);

  const loc = getLocale(client);

  // Remove duplicates then map the quoted posts
  [...new Set(messages)]
    .filter((p) => p !== undefined)
    .map((quoted_post) => {
      const embed = new EmbedBuilder().setColor("#2f3136").setAuthor({
        name: "Citation",
        iconURL: quoted_post.author.displayAvatarURL(),
      });

      // Handle attachments
      if (quoted_post.attachments.size !== 0) {
        handleAttachments(loc, embed, quoted_post.attachments);
      }

      // Description as post content
      if (quoted_post.content) {
        // Only if content exists and length > 0
        embed.setDescription(quoted_post.content);
      }

      // Footer
      let footer = `Posté le ${showDate(
        message.guild?.preferredLocale ?? client.config.default_lang,
        loc,
        quoted_post.createdAt,
      )}`;
      if (quoted_post.editedAt) {
        footer += ` et modifié le ${showDate(
          message.guild?.preferredLocale ?? client.config.default_lang,
          loc,
          quoted_post.editedAt,
        )}`;
      }

      let author = "Auteur";
      if (message.author === quoted_post.author) {
        author += " & Citateur";
      } else {
        footer += `\nCité par ${userWithNickname(message.member!) ?? "?"} le ${showDate(
          message.guild?.preferredLocale ?? client.config.default_lang,
          loc,
          message.createdAt,
        )}`;
      }

      embed.setFooter({
        text: footer,
        iconURL: message.author.avatarURL() ?? undefined,
      });

      // Location/author of the quoted post
      embed.addFields(
        {
          name: author,
          value: `${quoted_post.author}`,
          inline: true,
        },
        {
          name: "Message",
          value: `${quoted_post.channel} - [Lien Message](${quoted_post.url})`,
          inline: true,
        },
      );

      // Delete source message if no content when removing links
      if (
        !message.content.replace(RegexC(regex, RegExpFlags.Global), "").trim() &&
        messages.length === urls.length &&
        !message.mentions.repliedUser &&
        message.channel.isSendable()
      ) {
        message.delete();
        return message.channel.send({ embeds: [embed] });
      } else {
        return message.reply({
          embeds: [embed],
          allowedMentions: {
            repliedUser: false,
          },
        });
      }
    });
};
