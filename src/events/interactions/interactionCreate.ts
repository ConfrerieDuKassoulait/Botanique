import { useMainPlayer } from "discord-player";
import { Client, Interaction, InteractionType, MessageFlags } from "discord.js";

import { getLocale } from "../../utils/locales";

/** https://discord.js.org/#/docs/discord.js/main/class/Client?scrollTo=e-interactionCreate */
export default (interaction: Interaction, client: Client) => {
  const loc = getLocale(client, interaction.locale);
  switch (interaction.type) {
    case InteractionType.ApplicationCommand: {
      const command = client.commands.list.get(interaction.commandName);
      if (!command) {
        return interaction.reply({
          content: loc.get("e_interacreate_no_command"),
          flags: [MessageFlags.Ephemeral],
        });
      }

      if (interaction.guild) {
        const player = useMainPlayer();
        const data = {
          guild: interaction.guild,
        };

        return player.context.provide(data, () => command.interaction(interaction, client));
      }

      return command.interaction(interaction, client);
    }

    case InteractionType.ModalSubmit: {
      const modal = client.modals.list.get(interaction.customId);
      if (!modal) {
        return interaction.reply({
          content: loc.get("e_interacreate_no_modal"),
          flags: [MessageFlags.Ephemeral],
        });
      }

      return modal.interaction(interaction, client);
    }

    case InteractionType.ApplicationCommandAutocomplete: {
      const command = client.commands.list.get(interaction.commandName);
      if (!command) {
        return console.error(loc.get("e_interacreate_no_command"));
      }

      if (command.autocomplete) {
        return command.autocomplete(interaction);
      }

      return console.error(loc.get("e_interacreate_no_autocomplete"));
    }

    default:
      break;
  }
};
