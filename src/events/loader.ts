import { PlayerEvents, useMainPlayer } from "discord-player";
import { Client } from "discord.js";
import { readdir } from "fs/promises";

import { isDev, splitFilenameExtensions } from "../utils/misc";

/** Load all the events */
export default async (client: Client) => {
  const events_categories = (await readdir(__dirname, { withFileTypes: true }))
    .filter((element) => element.isDirectory())
    .map((element) => element.name);

  const player = useMainPlayer();

  if (isDev) {
    player.on("debug", async (message) => {
      console.log(`General player debug event: ${message}`);
    });

    player.events.on("debug", async (_, message) => {
      console.log(`Player debug event: ${message}`);
    });
  }

  player.events.on("error", (_, error) => {
    // Emitted when the player queue encounters error
    console.error(`General player error event: ${error.message}`);
    console.error(error);
  });

  player.events.on("playerError", (_, error) => {
    // Emitted when the audio player errors while streaming audio track
    console.error(`Player error event: ${error.message}`);
    console.error(error);
  });

  events_categories.forEach(async (event_category) => {
    // Retrieve events
    const events = await readdir(`${__dirname}/${event_category}`);

    // Load them into the client
    Promise.all(
      events.map(async (event_file) => {
        const { once, default: execute } = await import(
          `../events/${event_category}/${event_file}`
        );

        // Remove extension
        const { file: event_type, ext } = splitFilenameExtensions(event_file)!;
        if (!(ext === "js" || ext === "ts")) {
          throw `Unknown file in ${event_category}: ${event_file}`;
        }

        if (event_category === "player") {
          if (once) {
            // eslint-disable-next-line
            return player.events.once(event_type as keyof PlayerEvents, (...args: any[]) => {
              execute(...args, client);
            });
          }
          // eslint-disable-next-line
          return player.events.on(event_type as keyof PlayerEvents, (...args: any[]) => {
            execute(...args, client);
          });
        }

        if (once) {
          return client.once(event_type, (...args) => {
            execute(...args, client);
          });
        }
        return client.on(event_type, (...args) => {
          execute(...args, client);
        });
      }),
    );
  });
};
