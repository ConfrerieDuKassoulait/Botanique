import { GuildQueue, Track } from "discord-player";
import { Client } from "discord.js";
import { v4 as uuidv4 } from "uuid";

import { collect } from "../../buttons/loader";
import { embedNowPlaying, musicButtons } from "../../utils/commands/music";
import { musicLyrics, musicPlayResume } from "../../utils/constants";
import { getLocale } from "../../utils/locales";
import { Metadata } from "../../utils/metadata";

/** https://discord-player.js.org/docs/types/discord-player/GuildQueueEvents */
export default async (queue: GuildQueue<Metadata>, track: Track, client: Client) => {
  const loc = getLocale(client, queue.metadata.interaction?.locale);

  const embed = embedNowPlaying(track, loc);

  const idPauseResume = musicPlayResume + uuidv4();
  const idLyrics = musicLyrics + uuidv4();

  if (queue.metadata.interaction?.channel?.isSendable()) {
    const message = await queue.metadata?.interaction?.channel.send({
      embeds: [embed],
      components: [musicButtons(loc, queue, idPauseResume, idLyrics)],
    });

    // Buttons interactions
    collect(client, queue.metadata.interaction!, idPauseResume, message);
    collect(client, queue.metadata.interaction!, idLyrics, message);
  }
};
