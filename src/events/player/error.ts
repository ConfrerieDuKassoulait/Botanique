import { GuildQueue } from "discord-player";

import { Metadata } from "../../utils/metadata";

/** https://discord-player.js.org/docs/main/master/typedef/PlayerEvents */
export default (_: GuildQueue<Metadata>, error: Error) => {
  console.error(error);
};
