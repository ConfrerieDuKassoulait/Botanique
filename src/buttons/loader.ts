import {
  ChatInputCommandInteraction,
  Client,
  Message,
  MessageComponentInteraction,
  MessageFlags,
} from "discord.js";
import { readdir } from "fs/promises";

import { getLocale } from "../utils/locales";
import { removeExtension } from "../utils/misc";
import { ButtonActionTypes } from "../utils/monads/ButtonAction";

export default async (client: Client) => {
  // Dossier des buttons
  const buttons_categories = (await readdir(__dirname, { withFileTypes: true }))
    .filter((element) => element.isDirectory())
    .map((element) => element.name);

  await Promise.all(
    // For each categorie
    buttons_categories.map(async (buttons_category) => {
      // Retrieve all the commands
      const button_files = await readdir(`${__dirname}/${buttons_category}`);

      // Add the category to the collection for the help command
      client.buttons.categories.set(buttons_category, button_files.map(removeExtension));

      // Add the button
      return Promise.all(
        button_files.map(async (button_file) => {
          const button = (await import(`../buttons/${buttons_category}/${button_file}`)).default;

          // Add it to the collection so the interaction will work
          client.buttons.list.set(button.data.name, button);
          return button.data;
        }),
      );
    }),
  );
};

/**
 * Collect interactions for buttons
 * @param client Client
 * @param interaction Chat interaction
 * @param id Button ID
 * @param message Message holding the buttons
 * @param deferUpdate defer update in case update take time
 */
export const collect = (
  client: Client,
  interaction: ChatInputCommandInteraction | MessageComponentInteraction,
  id: string,
  message: Message,
  deferUpdate = false,
) => {
  const loc = getLocale(client, interaction.locale);
  const button = client.buttons.list.get(id.split("_")[0]);

  if (!button) {
    interaction.reply({
      content: loc.get("e_interacreate_no_button"),
      flags: [MessageFlags.Ephemeral],
    });
  }

  const filter = (i: MessageComponentInteraction) => i.customId === id;
  const collector = message.createMessageComponentCollector({
    filter,
    max: 1,
  });
  collector?.on("collect", async (i) => {
    if (deferUpdate) {
      await i.deferUpdate();
    }
    const msg = await button?.interaction(i, client);
    if (msg !== undefined) {
      const result = msg.unwrap();
      switch (result.type) {
        case ButtonActionTypes.RemoveComponents:
          message.edit({ components: [] });
          break;

        case ButtonActionTypes.EditMessage:
          await i.update(result.message);
          break;
      }
    }
  });
};
