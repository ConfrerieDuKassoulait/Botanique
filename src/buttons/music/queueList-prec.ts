import { useQueue } from "discord-player";
import {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  Client,
  EmbedBuilder,
  MessageComponentInteraction,
} from "discord.js";
import { v4 as uuidv4 } from "uuid";

import { embedListQueue } from "../../utils/commands/music";
import { musicQueueNext, musicQueuePrec } from "../../utils/constants";
import { getLocale } from "../../utils/locales";
import { getFilename } from "../../utils/misc";
import { ButtonAction, ButtonActionTypes } from "../../utils/monads/ButtonAction";
import { collect } from "../loader";

export default {
  data: {
    name: getFilename(__filename),
  },
  interaction: async (interaction: MessageComponentInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);
    const embed_desc = interaction.message.embeds.at(0)!.author!.name;

    // Retrieve Pages
    const pageMax = Number(/(\d+)(?!.*\d)/gm.exec(embed_desc)?.[0]);
    let page = Number(/(?!• \s+)\d(?=\/)/gm.exec(embed_desc)?.[0]);
    if (page - 1 === 0) {
      page = pageMax;
    } else {
      page--;
    }

    // Get queue
    const queue = useQueue(interaction.guildId!);

    const embed = new EmbedBuilder();
    const rows = [];
    if (queue) {
      // Create the embed
      embedListQueue(client, embed, queue, page, interaction.locale);

      // Create buttons
      const idPrec = musicQueuePrec + uuidv4();
      const idNext = musicQueueNext + uuidv4();
      rows.push(
        new ActionRowBuilder<ButtonBuilder>()
          .addComponents(
            new ButtonBuilder()
              .setCustomId(idPrec)
              .setLabel(loc.get("c_queue8"))
              .setStyle(ButtonStyle.Primary),
          )
          .addComponents(
            new ButtonBuilder()
              .setCustomId(idNext)
              .setLabel(loc.get("c_queue9"))
              .setStyle(ButtonStyle.Primary),
          ),
      );

      // Buttons interactions
      collect(client, interaction, idPrec, interaction.message);
      collect(client, interaction, idNext, interaction.message);
    } else {
      // In case queue doesn't exists
      embed.setDescription(loc.get("c_queue2"));
    }

    return new ButtonAction({
      type: ButtonActionTypes.EditMessage,
      message: {
        embeds: [embed],
        components: rows,
      },
    });
  },
};
