import { useMainPlayer, useQueue } from "discord-player";
import { Client, MessageComponentInteraction } from "discord.js";
import { v4 as uuidv4 } from "uuid";

import {
  embedLyrics,
  embedNowPlaying,
  findLyricsFromPlayer,
  musicButtons,
} from "../../utils/commands/music";
import {
  discord_limit_embed_per_message,
  musicLyrics,
  musicPlayResume,
} from "../../utils/constants";
import { getLocale } from "../../utils/locales";
import { getFilename } from "../../utils/misc";
import { ButtonAction, ButtonActionTypes } from "../../utils/monads/ButtonAction";
import { collect } from "../loader";

export default {
  data: {
    name: getFilename(__filename),
  },
  interaction: async (interaction: MessageComponentInteraction, client: Client) => {
    const queue = useQueue(interaction.guildId!);

    if (queue && queue.currentTrack && interaction.channel?.isSendable()) {
      const loc = getLocale(client, interaction.locale);

      const idPauseResume = musicPlayResume + uuidv4();
      const idLyrics = musicLyrics + uuidv4();

      collect(client, interaction, idPauseResume, interaction.message);
      collect(client, interaction, idLyrics, interaction.message);

      const data = await findLyricsFromPlayer(useMainPlayer(), queue.currentTrack);
      const embeds = embedLyrics(data[0]);
      if (embeds.length === 0) {
        interaction.message.reply({
          content: `${loc.get("c_lyrics2")} \`${queue.currentTrack.title}\``,
        });
      } else {
        interaction.channel?.send({ embeds: embeds.slice(0, discord_limit_embed_per_message) });
      }

      return new ButtonAction({
        type: ButtonActionTypes.EditMessage,
        message: {
          embeds: [embedNowPlaying(queue.currentTrack, loc)],
          components: [musicButtons(loc, queue, idPauseResume, idLyrics)],
        },
      });
    }

    interaction.deferUpdate({ withResponse: false });
    return new ButtonAction({ type: ButtonActionTypes.RemoveComponents });
  },
};
