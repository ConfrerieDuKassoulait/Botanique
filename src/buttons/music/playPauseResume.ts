import { useQueue } from "discord-player";
import { Client, MessageComponentInteraction } from "discord.js";
import { v4 as uuidv4 } from "uuid";

import { embedNowPlaying, musicButtons, toggleMusicPause } from "../../utils/commands/music";
import { musicLyrics, musicPlayResume } from "../../utils/constants";
import { getLocale } from "../../utils/locales";
import { getFilename } from "../../utils/misc";
import { ButtonAction, ButtonActionTypes } from "../../utils/monads/ButtonAction";
import { collect } from "../loader";

export default {
  data: {
    name: getFilename(__filename),
  },
  interaction: async (interaction: MessageComponentInteraction, client: Client) => {
    // Get queue
    const queue = useQueue(interaction.guildId!);
    if (queue && queue.currentTrack) {
      const loc = getLocale(client, interaction.locale);

      const idPauseResume = musicPlayResume + uuidv4();
      const idLyrics = musicLyrics + uuidv4();

      toggleMusicPause(queue);

      collect(client, interaction, idPauseResume, interaction.message);
      collect(client, interaction, idLyrics, interaction.message);

      return new ButtonAction({
        type: ButtonActionTypes.EditMessage,
        message: {
          embeds: [embedNowPlaying(queue.currentTrack, loc)],
          components: [musicButtons(loc, queue, idPauseResume, idLyrics)],
        },
      });
    }

    interaction.deferUpdate({ withResponse: false });
    return new ButtonAction({ type: ButtonActionTypes.RemoveComponents });
  },
};
