import {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  Client,
  MessageComponentInteraction,
} from "discord.js";
import { v4 as uuidv4 } from "uuid";

import { embedListReminders } from "../../utils/commands/reminder";
import { reminderListNext, reminderListPrec } from "../../utils/constants";
import { getLocale } from "../../utils/locales";
import { getFilename } from "../../utils/misc";
import { ButtonAction, ButtonActionTypes } from "../../utils/monads/ButtonAction";
import { collect } from "../loader";

export default {
  data: {
    name: getFilename(__filename),
  },
  interaction: async (interaction: MessageComponentInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);
    const embed_desc = interaction.message.embeds.at(0)?.description;

    // Retrieve Pages
    const pageMax = Number(/(\d+)(?!.*\d)/gm.exec(embed_desc!)?.[0]);
    let page = Number(/(?!• \s+)\d(?=\/)/gm.exec(embed_desc!)?.[0]);
    if (page - 1 === 0) {
      page = pageMax;
    } else {
      page--;
    }

    // Retrieve user
    const userId = /(?!<@)\d+(?=>)/gm.exec(embed_desc!)?.[0];
    const user = client.users.cache.get(userId!)!;

    // Fetch list
    const list = await embedListReminders(
      client,
      user,
      interaction.guildId,
      page,
      interaction.locale,
    );

    const idPrec = reminderListPrec + uuidv4();
    const idNext = reminderListNext + uuidv4();
    const row = new ActionRowBuilder<ButtonBuilder>()
      .addComponents(
        new ButtonBuilder()
          .setCustomId(idPrec)
          .setLabel(loc.get("c_reminder12"))
          .setStyle(ButtonStyle.Primary),
      )
      .addComponents(
        new ButtonBuilder()
          .setCustomId(idNext)
          .setLabel(loc.get("c_reminder13"))
          .setStyle(ButtonStyle.Primary),
      );

    // Buttons interactions
    collect(client, interaction, idPrec, interaction.message);
    collect(client, interaction, idNext, interaction.message);

    return new ButtonAction({
      type: ButtonActionTypes.EditMessage,
      message: {
        embeds: [list],
        components: [row],
      },
    });
  },
};
