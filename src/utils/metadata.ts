import { ChatInputCommandInteraction } from "discord.js";

export type Metadata = {
  interaction: ChatInputCommandInteraction | null;
};
