import { Client, Colors, EmbedBuilder, User } from "discord.js";

import { readSQL } from "../db";
import { getLocale } from "../locales";
import { blank, cleanCodeBlock } from "../misc";
import { RegexC, RegExpFlags } from "../regex";
import { showDate, strToSeconds, timeDeltaToString } from "../time";

/**
 * Option possible for reminders
 */
export enum OptionReminder {
  /** No parameters */
  Nothing,
  /** @ */
  Mention,
  /** p */
  DirectMessage,
}

/**
 * Store data about the remidner
 */
export type infoReminder = {
  locale: string;
  message: string | null;
  createdAt: number;
  channelId: string | null;
  userId: string;
  guildId: string | null;
};

export type dbReminder = {
  id: number;
  data: string | null;
  expiration_date: number;
  option_id: OptionReminder;
  channel_id: string | null;
  creation_date: string;
  user_id: string;
  guild_id: string | null;
  locale: string;
  timeout_id: string;
};

/**
 * Split the time and the extra args `p` and `@`
 * @param time raw text from user
 * @returns An object with the time and the option
 */
export const splitTime = (time: string) => {
  const mapping = {
    [OptionReminder.DirectMessage]: "p",
    [OptionReminder.Mention]: "@",
  };

  const trimmed = time.replaceAll(
    RegexC(Object.values(mapping).join("|"), RegExpFlags.Global | RegExpFlags.Insensitive),
    "",
  );

  // Depending of the last character of the string
  switch (time.toLowerCase().slice(-1)) {
    case mapping[OptionReminder.Mention]:
      return { time: trimmed, option: OptionReminder.Mention };
    case mapping[OptionReminder.DirectMessage]:
      return { time: trimmed, option: OptionReminder.DirectMessage };
    default:
      return { time: time, option: OptionReminder.Nothing };
  }
};

/**
 * Create a new reminder
 * @param client Client
 * @param time raw text from user about the time wanted
 * @param info data about the context of the reminder
 * @returns Promise resolution of the sql request
 */
export const newReminder = async (client: Client, time: string, info: infoReminder) =>
  new Promise((ok, ko) => {
    const loc = getLocale(client, info.locale);

    const data = splitTime(time);
    const timeout = strToSeconds(data.time);
    if (timeout < 0) {
      ko(loc.get("c_reminder18"));
      return;
    }

    const timeoutId = setTimeoutReminder(client, info, data.option, timeout);

    const expiration_date = info.createdAt + timeout * 1000;

    // Add the remind to the db
    client.db.run(
      readSQL("reminder/add"),
      [
        info.message,
        `${expiration_date}`,
        data.option.valueOf(),
        info.channelId,
        `${info.createdAt}`,
        info.userId,
        info.guildId,
        info.locale,
        timeoutId,
      ],
      (err) => {
        if (err) {
          ko(err);
        }

        // Send confirmation to user
        ok(`${loc.get("c_reminder1")} ${timeDeltaToString(expiration_date)}.`);
      },
    );
  });

/**
 * Delete a reminder from the database
 * @param client Client
 * @param createdAt Creation of the reminder
 * @param userId User ID who created the reminder
 * @returns what the SQlite request sended
 */
export const deleteReminder = (client: Client, createdAt: string, userId: string) => {
  // Delete the reminder for the database
  return new Promise((ok, ko) => {
    // Remove the remind to the db
    client.db.run(readSQL("reminder/remove"), [createdAt, userId], (err) => {
      if (err) {
        ko(err);
      }

      // Send confirmation to user
      ok(true);
    });
  });
};

export const sendReminder = (client: Client, info: infoReminder, option: OptionReminder) =>
  new Promise((resolve, reject) => {
    try {
      resolve(sendReminderAux(client, info, option));
    } catch (error) {
      reject(error);
    }
  });

const sendReminderAux = (client: Client, info: infoReminder, option: OptionReminder) => {
  const loc = getLocale(client, info.locale);
  // Send the message in the appropriate channel
  let message: string;
  if (info.message === null || info.message.length === 0) {
    message = loc.get("c_reminder7");
  } else {
    message = cleanCodeBlock(info.message);
  }
  const embed = new EmbedBuilder()
    .setColor("Random")
    .setDescription(message)
    .setTimestamp(info.createdAt);

  let channelOk = false;
  if (info.channelId !== null) {
    if (client.channels.cache.get(info.channelId) !== undefined) {
      channelOk = true;
    } else {
      embed.setFooter({ text: loc.get("c_reminder14") });
    }
  }

  let guildOk = false;
  if (info.guildId !== null) {
    const guild = client.guilds.cache.get(info.guildId);
    if (guild !== undefined) {
      if (guild.members.cache.get(info.userId) !== undefined) {
        guildOk = true;
      } else {
        embed.setFooter({ text: `${loc.get("c_reminder15")} ${guild.name}.` });
      }
    } else {
      embed.setFooter({ text: loc.get("c_reminder16") });
    }
  }

  if (option === OptionReminder.DirectMessage || !channelOk || !guildOk) {
    // Direct message
    const user = client.users.cache.get(info.userId);
    if (user !== undefined) {
      user.send({ embeds: [embed] });
    }
  } else {
    // Channel
    client.channels.fetch(info.channelId!).then((channel) => {
      if (channel?.isSendable()) {
        const author_mention = `<@${info.userId}>`;

        let content = author_mention;
        embed.setFooter({
          text: `${loc.get("c_reminder17")} ${timeDeltaToString(info.createdAt)}`,
        });

        // Mention everybody if needed
        if (option === OptionReminder.Mention) {
          [...new Set(info.message?.match(/<@\d+>/g) ?? [])]
            .filter((mention) => mention !== author_mention)
            .forEach((mention: string) => {
              content += " " + mention;
            });
        }

        channel.send({ content, embeds: [embed] });
      }
    });
  }
};

/**
 * Create a timeout for a reminder
 * @param client Client
 * @param info info about the reminder
 * @param option option used for this reminder (aka location of the response)
 * @param timeout Amout of time before the reminder ends
 * @returns Timeout's ID
 */
export const setTimeoutReminder = (
  client: Client,
  info: infoReminder,
  option: OptionReminder,
  timeout: number,
) => {
  const setChunkedTimeout = (remainingTime: number) => {
    // Maximum for setTimeout is Int32
    if (remainingTime > 2147483647) {
      // Schedule a 24-hour delay (24 * 60 * 60 * 1000), then check again
      const dayChunk = 86400000;

      return setTimeout(() => {
        setChunkedTimeout(remainingTime - dayChunk);
      }, dayChunk);
    }

    // Final timeout when remaining time is within limit
    return setTimeout(() => {
      deleteReminder(client, String(info.createdAt), info.userId).then((val) => {
        if (val != true) {
          throw val;
        }

        sendReminder(client, info, option);
      });
    }, remainingTime);
  };

  // Convert to milliseconds
  return Number(setChunkedTimeout(timeout * 1000));
};

/**
 * Check the owernship of a reminder by a user
 * @param client Client
 * @param id ID of the reminder
 * @param userId user ID to check
 * @param guildId guild ID where the ownership request as been send, 0 if DM
 */
export const checkOwnershipReminder = async (
  client: Client,
  id: number,
  userId: string,
  guildId: string,
) => {
  type returnData = { [key: string]: number };

  const data = (await new Promise((ok, ko) => {
    // Check the ownership
    client.db.all<returnData>(
      readSQL("reminder/ownership_check"),
      [id, userId, guildId],
      (err, row) => {
        if (err) {
          ko(err);
        }

        // Send all the current reminders
        ok(row[0]);
      },
    );
  })) as returnData;
  return Object.keys(data).map((key) => data[key])[0] === 0 ? true : false;
};

/**
 * Retrieve info about a reminder
 * @param client Client
 * @param id Reminder's ID
 */
export const getReminderInfo = async (client: Client, id: number) => {
  return (await new Promise((ok, ko) => {
    // Check the ownership
    client.db.all<dbReminder>(readSQL("reminder/findById"), [id], (err, row) => {
      if (err) {
        ko(err);
      }

      // Send all the current reminders
      ok(row[0]);
    });
  })) as dbReminder;
};

/**
 * Update an entry of the database
 * @param client Client
 * @param data Data who will override the data in database
 */
export const updateReminder = (client: Client, data: dbReminder) => {
  // Delete the reminder for the database
  return new Promise((ok, ko) => {
    // Update the db
    client.db.run(
      readSQL("reminder/update"),
      [
        data.data,
        data.expiration_date,
        data.option_id,
        data.channel_id,
        data.creation_date,
        data.user_id,
        data.guild_id,
        data.locale,
        data.timeout_id,
        data.id,
      ],
      (err) => {
        if (err) {
          ko(err);
        }

        ok(true);
      },
    );
  });
};

/**
 * Return a list of reminders for a user in a specified context
 * @param client Client
 * @param userId user ID
 * @param guildId guild ID
 * @returns List of reminders of a user in a guild
 */
const listReminders = async (client: Client, userId: string, guildId: string | null) => {
  return (await new Promise((ok, ko) => {
    // Check the ownership
    client.db.all<dbReminder>(readSQL("reminder/find"), [userId, guildId ?? 0], (err, row) => {
      if (err) {
        ko(err);
      }

      // Send all the current reminders
      ok(row);
    });
  })) as dbReminder[];
};

/**
 * Return the embed of the reminders
 * @param client Client
 * @param user User
 * @param guildId Guild ID
 * @param page Page requested
 * @param local Lang
 * @returns Pretty embed who list reminders
 */
export const embedListReminders = async (
  client: Client,
  user: User,
  guildId: string | null,
  page: number,
  local: string,
) => {
  const loc = getLocale(client, local);
  const reminders = await listReminders(client, user.id, guildId);

  const elementPerPage = 5;
  const pageMax = Math.ceil(reminders.length / elementPerPage);

  if (pageMax <= 1) {
    page = 1;
  }
  // TODO: Use Random color or force a color from args
  const embed = new EmbedBuilder()
    .setColor(Colors.DarkGrey)
    .setDescription(
      `${loc.get("c_reminder5")} ${user} • ${loc.get("c_reminder6")} ${page}/${pageMax}`,
    )
    .setThumbnail(user.displayAvatarURL());

  const limit = elementPerPage * page;

  if (reminders.length > 0 && page <= pageMax) {
    let curseur = limit - elementPerPage;
    reminders.splice(0, limit - elementPerPage);
    reminders.forEach((remind) => {
      if (curseur < limit) {
        let text = remind.data ?? loc.get("c_reminder7");
        if (text.length > 1024) {
          text = `${text.substring(0, 1021)}...`;
        }
        const expiration = `${loc.get("c_reminder8")} ${timeDeltaToString(remind.expiration_date)}`;
        embed.addFields({
          name: `#${remind.id} • ${loc.get("c_reminder9")} ${showDate(
            local,
            loc,
            new Date(Number(remind.creation_date)),
          )}\n${expiration}`,
          value: text,
          inline: false,
        });

        curseur++;
      }
    });
  } else {
    embed.addFields({
      name: blank,
      value: `${loc.get("c_reminder10")}${page} ${loc.get("c_reminder11")}.`,
    });
  }

  return embed;
};
