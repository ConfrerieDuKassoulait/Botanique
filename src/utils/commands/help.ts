import {
  APIApplicationCommandSubcommandOption,
  ApplicationCommandOptionType,
  Client,
  Locale,
  SlashCommandBuilder,
} from "discord.js";

type Data = SlashCommandBuilder | APIApplicationCommandSubcommandOption;

/**
 * Find the name of the command, trying to get the correct locale
 * @param data Command data
 * @param locale Locale wanted
 * @returns Command's name
 */
export const goodName = (data: Data, locale: Locale) =>
  data.name_localizations?.[locale] ?? data.name;

/**
 * Find the description of the command, trying to get the correct locale
 * @param data Command data
 * @param locale Locale wanted
 * @returns Command's description
 */
export const goodDescription = (data: Data, locale: Locale) =>
  data.description_localizations?.[locale] ?? data.description;

/**
 * Aux function for Sub/NameNotLocalized
 * @param cmd data
 * @param command command researched
 * @returns if we found or not the researched command
 */
const filterLocalizations = (cmd: Data, command: string) => {
  let res = false;
  for (const key in cmd?.name_localizations) {
    res = res || cmd.name_localizations?.[key as Locale] === command;
  }

  return res;
};

/**
 * Find a command based on any string, localized or not
 * @param command string
 * @returns the not localized corresponding string's command name
 */
export const NameNotLocalized = (client: Client, command: string): SlashCommandBuilder | null => {
  const list = client.commands.list.map((cmd) => cmd.data);

  return (
    list.find((cmd) => cmd.name === command) ||
    list.filter((cmd) => filterLocalizations(cmd, command))[0]
  );
};

/**
 * Find a subcommand of a command based on any string, localized or not
 * @param parent command of the subcommand
 * @param command string
 * @returns the not localized corresponding string's subcommand name
 */
export const SubnameNotLocalized = (parent: SlashCommandBuilder, command: string) => {
  const list = parent
    ?.toJSON()
    .options?.filter((option) => option.type === ApplicationCommandOptionType.Subcommand);

  return (
    list?.find((cmd) => cmd?.name === command) ||
    list?.filter((cmd) => filterLocalizations(cmd, command))[0]
  );
};
