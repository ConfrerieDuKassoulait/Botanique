import { GuildQueue, LrcSearchResult, Player, QueueRepeatMode, Track } from "discord-player";
import { ActionRowBuilder, ButtonBuilder, ButtonStyle, Client } from "discord.js";

import { EmbedBuilder } from "@discordjs/builders";

import { discord_limit_embed_description_length, discord_limit_embed_field } from "../constants";
import { getLocale } from "../locales";
import { blank, emojiPng } from "../misc";
import { RegexC } from "../regex";

export const embedListQueue = (
  client: Client,
  embed: EmbedBuilder,
  queue: GuildQueue,
  page: number,
  local: string,
) => {
  const loc = getLocale(client, local);
  const tracks = queue.tracks.toArray();

  // Add the current song at the top of the list
  tracks.unshift(queue.history.currentTrack!);

  const limit_fields = discord_limit_embed_field;

  const pageMax = Math.ceil(tracks.length / limit_fields);

  embed.setAuthor({ name: `${loc.get("c_queue1")} • ${loc.get("c_queue7")} ${page}/${pageMax}` });
  embed.setFooter({ text: `${printRepeatMode(queue.repeatMode, loc)}` });

  tracks.slice((page - 1) * limit_fields, page * limit_fields).forEach((t, idx) => {
    const now_playing = idx === 0 && page === 1;
    const name = now_playing
      ? loc.get("c_queue10")
      : (idx === 1 && page === 1) || (idx === 0 && page > 1)
        ? loc.get("c_queue11")
        : blank;
    const idx_track = now_playing ? "" : `${idx + limit_fields * (page - 1)}. `;
    embed.addFields({
      name,
      value: `${idx_track}[${t.title} • ${t.author}](${t.url}) (${t.duration})`,
    });
  });
};

const printRepeatMode = (mode: QueueRepeatMode, loc: Map<string, string>) => {
  switch (mode) {
    case QueueRepeatMode.OFF:
      return loc.get("c_repeat2");

    case QueueRepeatMode.QUEUE:
      return loc.get("c_repeat3") + " " + loc.get("c_repeat6");

    case QueueRepeatMode.AUTOPLAY:
      return loc.get("c_repeat4") + " " + loc.get("c_repeat6");

    case QueueRepeatMode.TRACK:
      return loc.get("c_repeat5") + " " + loc.get("c_repeat6");

    default:
      break;
  }
};

export enum QueueStatus {
  /** Currently playing */
  Play = 0,
  /** Currently in pause */
  Pause = 1,
}

/**
 * Change the status of the queue
 * @param queue Queue
 * @returns Current status, after the changes
 */
export const toggleMusicPause = (queue: GuildQueue) => {
  if (queue.node.isPaused()) {
    queue.node.resume();
    return QueueStatus.Play;
  }

  queue.node.pause();
  return QueueStatus.Pause;
};

export const embedNowPlaying = (track: Track, loc: Map<string, string>) => {
  return new EmbedBuilder()
    .setDescription(`${loc?.get("e_trackstart1")} ${track.requestedBy}`)
    .setTitle(track.title + " • " + track.author)
    .setURL(track.url)
    .setThumbnail(track.thumbnail)
    .setFooter({
      text: `${loc?.get("e_trackstart2")} ${track.duration} via ${track.source.capitalize()}`,
      iconURL: emojiPng("🎶"),
    });
};

export const embedLyrics = (title: LrcSearchResult) => {
  if (title === undefined || title.plainLyrics == null) {
    return [];
  }

  const nb_embed = Math.ceil(title.plainLyrics.length / discord_limit_embed_description_length);

  /* https://git.mylloon.fr/ConfrerieDuKassoulait/Botanique/issues/186
   * TODO: If lyrics < 6000, only send one message with multiples embed
   *       + Better cut in lyrics */

  const embeds = [];
  for (let i = 0, j = 0; i < nb_embed; i++, j += discord_limit_embed_description_length) {
    const lyrics = title.plainLyrics.slice(j, j + discord_limit_embed_description_length);

    const embed = new EmbedBuilder();
    switch (i) {
      case 0: {
        // First embed
        embed
          .setTitle(title.trackName)
          .setAuthor({ name: title.artistName })
          .setDescription(lyrics);
        break;
      }

      case nb_embed - 1: {
        // Footer of last embed in case of multiple embed
        embed.setDescription(lyrics).setFooter({
          text: `${title.artistName} · ${title.trackName}`,
        });
        break;
      }

      default: {
        // Embed with only lyrics in case of multiple embed
        embed.setDescription(lyrics);
        break;
      }
    }

    embeds.push(embed);
  }
  return embeds;
};

export const findLyricsFromPlayer = async (player: Player, track: Track) => {
  const request = track.cleanTitle + " " + track.author.replace(RegexC(/ - Topic$/), "");
  return await player.lyrics.search({
    q: request,
  });
};

export const musicButtons = (
  loc: Map<string, string>,
  queue: GuildQueue,
  idPauseResume: string,
  idLyrics: string,
) => {
  return new ActionRowBuilder<ButtonBuilder>()
    .addComponents(
      new ButtonBuilder()
        .setCustomId(idPauseResume)
        .setLabel(queue.node.isPaused() ? loc.get("c_pause5")! : loc.get("c_pause4")!)
        .setStyle(queue.node.isPaused() ? ButtonStyle.Primary : ButtonStyle.Secondary),
    )
    .addComponents(
      new ButtonBuilder()
        .setCustomId(idLyrics)
        .setLabel(loc.get("c_lyrics_name")!.capitalize())
        .setStyle(ButtonStyle.Secondary),
    );
};
