/** Max message length */
export const discord_limit_message = 2000;

/** Max embed description length */
export const discord_limit_embed_description_length = 4096;

/** Max embed field an embed can have */
export const discord_limit_embed_field = 25;

/** Max element the autocompletion of slash commands can have */
export const discord_limit_autocompletion_list_length = 25;

/** Max length of an element in autocompletion of slash commands */
export const discord_limit_autocompletion_value_length = 100;

/** Max embeds per message */
export const discord_limit_embed_per_message = 10;

/** Last page in the music queue */
export const musicQueuePrec = "queueList-prec_";

/** Next page in the music queue */
export const musicQueueNext = "queueList-next_";

/** Last page in the reminder list */
export const reminderListPrec = "reminderList-prec_";

/** Next page in the reminder queue */
export const reminderListNext = "reminderList-next_";

/** Pause or resume the current track */
export const musicPlayResume = "playPauseResume_";

/** Print lyrics of the current track */
export const musicLyrics = "playLyrics_";
