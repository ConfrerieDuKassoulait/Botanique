export enum RegExpFlags {
  // Global
  Global = 1 << 0,
  // Multi Line
  MultiLine = 1 << 1,
  // Ignore Case
  Insensitive = 1 << 2,
  // Sticky
  Sticky = 1 << 3,
  // Unicode
  Unicode = 1 << 4,
  // Dot All
  SingleLine = 1 << 6,
}

const flagsToString = (flags: number) => {
  let result = "";

  if (flags & RegExpFlags.Global) result += "g";
  if (flags & RegExpFlags.MultiLine) result += "m";
  if (flags & RegExpFlags.Insensitive) result += "i";
  if (flags & RegExpFlags.Sticky) result += "y";
  if (flags & RegExpFlags.Unicode) result += "u";
  if (flags & RegExpFlags.SingleLine) result += "s";

  return result;
};

export const RegexC = (pattern: RegExp | string, flags: number = 0) =>
  new RegExp(pattern, flagsToString(flags));
