import { InteractionUpdateOptions } from "discord.js";

export enum ButtonActionTypes {
  RemoveComponents,
  EditMessage,
  ChangeNothing,
}

type UnitType = {
  type: ButtonActionTypes.RemoveComponents | ButtonActionTypes.ChangeNothing;
};

type MessageType = {
  type: ButtonActionTypes.EditMessage;
  message: InteractionUpdateOptions;
};

type ButtonActionPossibilities = UnitType | MessageType;

export class ButtonAction {
  constructor(private readonly action: ButtonActionPossibilities) {
    this.action = action;
  }

  unwrap(): ButtonActionPossibilities {
    return this.action;
  }
}
