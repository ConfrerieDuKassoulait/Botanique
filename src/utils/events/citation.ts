import { APIEmbedField, Attachment, Collection, EmbedBuilder } from "discord.js";

import { isImage } from "../misc";

export const handleAttachments = (
  loc: Map<string, string>,
  embed: EmbedBuilder,
  attachments: Collection<string, Attachment>,
) => {
  if (attachments.size === 1 && isImage(attachments.first()!.name)) {
    // Only contains one image
    embed.setImage(attachments.first()!.url);
  } else {
    // Contains more than one image and/or other files

    // We are currently losing a link to a file if the link is too long
    // We could truncate the filename ?
    const maxFieldValueLength = 1024;
    const files = attachments
      .map((file) => `[${file.name}](${file.url})`)
      .filter((link) => link.length <= maxFieldValueLength);

    let currentField = "";
    const fields: APIEmbedField[] = [];
    let multipleFields = 0;
    let numberOfLinks = 0;
    files.forEach((file, idx) => {
      numberOfLinks++;
      const fieldValue = currentField.length > 0 ? `${currentField}, ${file}` : file;

      if (fieldValue.length > maxFieldValueLength) {
        multipleFields = multipleFields === 0 && idx !== files.length - 1 ? 1 : multipleFields + 1;
        fields.push({
          name:
            loc.get(
              attachments.size > 1 && numberOfLinks > 1 ? "e_attachements" : "e_attachement",
            ) + (multipleFields ? ` (${multipleFields})` : ""),
          value: currentField,
        });
        currentField = file;
        numberOfLinks = 0;
      } else {
        currentField = fieldValue;
      }
    });

    if (currentField.length > 0) {
      fields.push({
        name:
          loc.get(attachments.size > 1 && numberOfLinks > 1 ? "e_attachements" : "e_attachement") +
          (multipleFields ? ` (${multipleFields + 1})` : ""),
        value: currentField,
      });
    }

    embed.addFields(fields);
  }
};
