import "../modules/client";

import { Player } from "discord-player";
import { YoutubeiExtractor } from "discord-player-youtubei";
import { ActivityType, Client, Collection, GatewayIntentBits } from "discord.js";
import { readFileSync } from "fs";
import { Database } from "sqlite3";

import { DefaultExtractors } from "@discord-player/extractor";

import { readSQL } from "./db";
import { loadLocales } from "./locales";
import { isDev } from "./misc";

/** Creation of the client and definition of its properties */
export default async () => {
  const activities = isDev ? [] : [{ name: "/help", type: ActivityType.Watching }];

  const client: Client = new Client({
    shards: "auto",
    intents: [
      GatewayIntentBits.Guilds,
      GatewayIntentBits.GuildMessages,
      GatewayIntentBits.MessageContent,
      GatewayIntentBits.GuildVoiceStates,
    ],
    presence: {
      activities,
    },
  });

  client.config = {
    version: JSON.parse(readFileSync("./package.json").toString()).version,
    token_discord: process.env.TOKEN_DISCORD,
    default_lang: process.env.DEFAULT_LANG ?? "fr",
  };

  client.modals = {
    categories: new Collection(),
    list: new Collection(),
  };

  client.buttons = {
    categories: new Collection(),
    list: new Collection(),
  };

  client.commands = {
    categories: new Collection(),
    list: new Collection(),
  };

  const player = new Player(client, { skipFFmpeg: true });

  await player.extractors.loadMulti(DefaultExtractors);
  await player.extractors.register(YoutubeiExtractor, {});

  console.log("Translations progression :");
  client.locales = await loadLocales(client.config.default_lang);

  client.db = new Database(`${process.env.DOCKERIZED === "1" ? "/config" : "./config"}/db.sqlite3`);

  client.db.run(readSQL("init"));

  return client;
};

/**
 * Quit gracefully the client
 * @param client Client
 */
export const quit = (client: Client) => {
  // Close DB
  client.db.close();

  // Close client
  client.destroy();
};
