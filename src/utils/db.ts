import fs from "node:fs";

import { isDev } from "./misc";

export const readSQL = (path: string) => {
  const root = isDev ? "./src" : "./dist";
  const dir = root + "/sql/";
  if (!path.startsWith(dir)) {
    path = dir + path;
  }

  const ext = ".sql";
  if (!path.endsWith(ext)) {
    path += ext;
  }

  return fs.readFileSync(path, "utf8");
};
