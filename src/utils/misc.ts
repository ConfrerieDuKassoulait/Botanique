import { GuildMember } from "discord.js";

/** Check if we are in the dev environnement */
export const isDev = process.env.NODE_ENV !== "production";

/**
 * Log module status
 * @param {string} name Module name
 * @param {boolean} status Module status
 * @returns String
 */
export const logStart = (name: string, status: boolean) => {
  // TODO Handle precision about the error if status is false
  return `> ${name}\t${status === true ? "✅" : "❌"}`;
};

/**
 * Filename without path and extension
 * @param path __filename
 * @returns string
 */
export const getFilename = (path: string) => {
  const path_list = path.split("/");

  // Check if filename exist
  const filename_with_ext = path_list.pop();
  if (filename_with_ext === undefined) {
    throw new Error(`Filename error: don't exist in ${path}`);
  }

  return removeExtension(filename_with_ext);
};

/**
 * Split a filename and his extension
 * @param filename string of the filename
 * @returns Object with filename and extension splitted
 */
export const splitFilenameExtensions = (filename: string) => {
  if (filename.length === 0) {
    return undefined;
  }

  // Check if the filename starts with a dot and has no other dots
  if (filename.startsWith(".") && filename.indexOf(".", 1) === -1) {
    return { file: filename, ext: undefined };
  }

  const lastDotIndex = filename.lastIndexOf(".");

  // If there's no dot or the dot is at the start, treat the whole string as the filename
  if (lastDotIndex <= 0) {
    return { file: filename, ext: undefined };
  }

  const file = filename.slice(0, lastDotIndex);
  const ext = filename.slice(lastDotIndex + 1);

  return { file, ext };
};

/**
 * Remove extension from a filename
 * @param filename string of the filename with an extension
 * @returns string of the filename without an extension
 */
export const removeExtension = (filename: string) => {
  return splitFilenameExtensions(filename)!.file;
};

/**
 * Define if a media is a media based on file extension
 * @param filename string of the filename
 * @returns true is file is a media
 */
export const isImage = (filename: string) => {
  return Boolean(
    splitFilenameExtensions(filename)
      ?.ext?.toLowerCase()
      .match(/jpg|jpeg|png|webp|gif/),
  );
};

/**
 * String with pseudo and nickname if available
 * @param member Member
 * @returns string
 */
export const userWithNickname = (member: GuildMember) => {
  if (!member) {
    return undefined;
  }
  if (member.nickname) {
    return `${member.nickname} (${member.user.tag})`;
  } else {
    return member.user.tag;
  }
};

/**
 * Move the text into backtick text, preserving mentions and links
 * @param text Text
 * @returns Formatted text
 */
export const cleanCodeBlock = (text: string) => {
  text = `\`${text.trim()}\``;

  // Keep mentions
  text = text.replace(/(<@\d+>)/g, function (mention: string) {
    return `\`${mention}\``;
  });

  // Keep links
  // Reference: https://stackoverflow.com/a/3809435/15436737
  text = text.replace(
    /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b[-a-zA-Z0-9()@:%_+.~#?&//=]*/g,
    function (url: string) {
      return `\`${url}\``;
    },
  );

  // Fix issues
  text = text.replaceAll("``", "");

  return text;
};

/**
 * Returns the emoji URL as png, only works with one-composed emoji code
 * @param emoji Emoji
 * @returns URL of emoji as png
 */
export const emojiPng = (emoji: string) =>
  `https://cdn.jsdelivr.net/gh/twitter/twemoji/assets/72x72/${emoji
    .codePointAt(0)
    ?.toString(16)}.png`;

/**
 * Blank character
 */
export const blank = "\u200b";
