import moment from "moment-timezone";

import { RegexC, RegExpFlags } from "./regex";

/**
 * Parsed string adapted with TZ (locales) and format for the specified lang
 * @param lang Locale
 * @param translation Translation for "at"
 * @param date Date
 * @returns String
 */
export const showDate = (lang: string, translation: Map<string, unknown>, date: Date) => {
  const localeInfo = new Intl.Locale(lang);
  const intlTimezone = moment.tz.zonesForCountry(localeInfo.region ?? localeInfo.baseName);
  const formattedDate = new Intl.DateTimeFormat(lang, {
    timeZone: intlTimezone ? intlTimezone[0] : "Etc/GMT",
    dateStyle: "short",
    timeStyle: "medium",
  })
    .format(date)
    .split(" ");

  return `${formattedDate[0]} ${translation.get("u_time_at")} ${formattedDate[1]}`;
};

export enum TimeSecond {
  Year = 60 * 60 * 24 * 365,
  Week = 60 * 60 * 24 * 7,
  Day = 60 * 60 * 24,
  Hour = 60 * 60,
  Minute = 60,
  Second = 1,
}

/**
 * Get next time unit. For example the next unit after Hour is Minute
 * @param currentUnit Current time unit
 * @returns The next time unit
 */
export const nextTimeUnit = (currentUnit: number) => {
  const units = Object.values(TimeSecond) as number[];

  const index = units.indexOf(currentUnit);
  return units[index + 1] || TimeSecond.Second;
};

/**
 * Take a cooldown, for example 2min and transform it to seconds, here: 120s
 * @param time time in human format
 * @returns time in seconds
 */
export const strToSeconds = (time: string) => {
  if (time.length > 15) {
    // 15 is a magic value as it's weird to have time this long
    return -1;
  }

  const noUnit = "unmarked";
  const regex = RegexC(
    `(?<${TimeSecond[TimeSecond.Year]}>[0-9]+(?=[y|a]))|` +
      `(?<${TimeSecond[TimeSecond.Week]}>[0-9]+(?=[w]))|` +
      `(?<${TimeSecond[TimeSecond.Day]}>[0-9]+(?=[d|j]))|` +
      `(?<${TimeSecond[TimeSecond.Hour]}>[0-9]+(?=[h]))|` +
      `(?<${TimeSecond[TimeSecond.Minute]}>[0-9]+(?=[m]))|` +
      `(?<${TimeSecond[TimeSecond.Second]}>[0-9]+(?=[s]))|` +
      `(?<${noUnit}>[0-9]+)`,
    RegExpFlags.Global | RegExpFlags.Insensitive,
  );

  const data = Array.from(time.matchAll(regex));
  if (data.length === 0) {
    // Regex returned an invalid time
    return -1;
  }

  let res = 0;
  let lastUnit = TimeSecond.Second;
  data.forEach((match) => {
    Object.entries(match.groups!).forEach(([key, value]) => {
      if (value) {
        let unit;
        if (key === noUnit) {
          unit = nextTimeUnit(lastUnit);
          res += +value * unit;
        } else {
          unit = TimeSecond[key as keyof typeof TimeSecond];
          res += +value * unit;
        }
        lastUnit = unit;
      }
    });
  });

  return res;
};

/**
 * Returns the time in a readable way
 * @param seconds Time in milliseconds
 * @returns Time as string
 */
export const timeToString = (time: number) => {
  let secondsDifference = Math.abs(Math.ceil(time / 1000));

  if (secondsDifference === 0) {
    return "0s";
  }

  return Object.entries(TimeSecond)
    .map(([key, value]) => ({
      label: key.charAt(0).toLowerCase(),
      value: value as TimeSecond,
    }))
    .map(({ label, value }) => {
      if (secondsDifference >= value) {
        const amount = Math.floor(secondsDifference / value);
        secondsDifference -= amount * value;
        return `${amount}${label}`;
      }
      return null;
    })
    .filter(Boolean)
    .join(" ");
};

/**
 * Calculating the difference between a date and now
 * @param time Time in milliseconds
 * @returns Delta between the time and now
 */
export const timeDeltaToString = (time: number) => {
  const now = Date.now();
  return timeToString(time - now);
};
