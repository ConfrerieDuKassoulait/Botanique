import { Collection } from "discord.js";
import { Database } from "sqlite3";

import { SlashCommandBuilder } from "@discordjs/builders";

import { ButtonAction } from "../utils/monads/ButtonAction";

declare module "discord.js" {
  export interface Client {
    /** Store the configuration */
    config: {
      /** Bot version */
      version: string;
      /** Bot token from env variable */
      token_discord: string | undefined;
      /** Default lang used */
      default_lang: string;
    };
    /** Store all the modals */
    modals: {
      categories: Collection<
        /** Category name */
        string,
        /** Name of the modals in the category */
        string[]
      >;
      list: Collection<
        /** Modal name */
        string,
        /** Modal itself */
        {
          /** Data about the modal */
          data: {
            name: string;
          };
          /** How the modal interact */
          interaction: (interaction: ModalSubmitInteraction, client: Client) => unknown;
        }
      >;
    };
    /** Store all the buttons */
    buttons: {
      categories: Collection<
        /** Category name */
        string,
        /** Name of the buttons in the category */
        string[]
      >;
      list: Collection<
        /** Button name */
        string,
        /** Button itself */
        {
          /** Data about the button */
          data: {
            name: string;
          };
          /** How the button interact */
          interaction: (
            interaction: MessageComponentInteraction,
            client: Client,
          ) => Promise<ButtonAction>;
        }
      >;
    };
    /** Store all the slash commands */
    commands: {
      categories: Collection<
        /** Category name */
        string,
        /** Name of the commands in the category */
        string[]
      >;
      list: Collection<
        /** Command name */
        string,
        /** Command itself */
        {
          /** Guilds where the command is active */
          scope: () => string[];
          /** Data about the command */
          data: SlashCommandBuilder;
          /** How the command interact */
          interaction: (interaction: CommandInteraction, client: Client) => unknown;
          /** Autocomplete logic */
          autocomplete: undefined | ((interaction: AutocompleteInteraction) => unknown);
        }
      >;
    };
    /** Store all the localizations */
    locales: Map<string, Map<string, string>>;
    db: Database;
  }
}
