export {};

declare global {
  // Declarations
  interface String {
    /**
     * Returns a copy of the string with the first letter capitalized
     */
    capitalize(): string;
  }
}

/** Capitalize definition */
String.prototype.capitalize = function (this: string) {
  if (this.length === 0) {
    return this;
  }

  return this[0].toUpperCase() + this.substring(1);
};
