export {};

declare module "discord-player" {
  export interface GuildQueue {
    syncedLyricsMemory:
      | {
          isSubscribed: () => unknown;
          unsubscribe: () => unknown;
        }
      | undefined;
  }
}
