import { isDev } from "./utils/misc";

/** Load the app */
const start_app = () => {
  import("./load").then((l) => l.run().catch((error) => console.error(error)));
};

// Load .env if not in prod
if (isDev) {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  import("dotenv").then((c) => {
    c.config({ path: "./config/.env" });

    start_app();
  });
} else {
  start_app();
}
