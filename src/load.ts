import loadButtons from "./buttons/loader";
import loadCommands from "./commands/loader";
import loadEvents from "./events/loader";
import loadModals from "./modals/loader";
import loadClient, { quit } from "./utils/client";
import { isDev, logStart } from "./utils/misc";

/** Run the bot */
export const run = async () => {
  console.log("Starting Botanique...");

  // Client Discord.JS
  const client_name = "Client";
  await loadClient()
    .then(async (client) => {
      if (isDev) {
        // Attach debugging listeners
        client.on("debug", console.log).on("warn", console.warn);
      }

      // Events Discord.JS and Player
      const events_name = "Events";
      await loadEvents(client)
        .then(() => console.log(logStart(events_name, true)))
        .catch((err) => {
          console.error(err);
          throw logStart(events_name, false);
        });

      // Connect the bot to Discord.com
      await client.login(client.config.token_discord);

      // Modals Discord.JS
      const modals_name = "Modals";
      await loadModals(client)
        .then(() => console.log(logStart(modals_name, true)))
        .catch((err) => {
          console.error(err);
          throw logStart(modals_name, false);
        });

      // Buttons Discord.JS
      const buttons_name = "Buttons";
      await loadButtons(client)
        .then(() => console.log(logStart(buttons_name, true)))
        .catch((err) => {
          console.error(err);
          throw logStart(buttons_name, false);
        });

      // Commands Slash Discord.JS
      const commands_name = "Commands";
      await loadCommands(client)
        .then(() => console.log(logStart(commands_name, true)))
        .catch((err) => {
          console.error(err);
          throw logStart(commands_name, false);
        });

      console.log(logStart(client_name, true));
      console.log(`Botanique "${client.user?.username}" v${client.config.version} started!`);

      // Handle quit
      process.on("exit", () => quit(client));
      process.on("SIGHUP", () => process.exit(128 + 1));
      process.on("SIGINT", () => process.exit(128 + 2));
      process.on("SIGTERM", () => process.exit(128 + 15));
    })
    .catch((err) => {
      console.error(err);
      throw logStart(client_name, false);
    });
};
