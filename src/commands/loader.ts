import { Routes } from "discord-api-types/v9";
import { Client } from "discord.js";
import { readdir } from "fs/promises";

import { REST } from "@discordjs/rest";

import { removeExtension } from "../utils/misc";

/** Load all the commands */
export default async (client: Client) => {
  const rest = new REST({ version: "10" }).setToken(client.token!);

  const command_categories = (await readdir(__dirname, { withFileTypes: true }))
    .filter((element) => element.isDirectory())
    .map((element) => element.name);

  const commands = (
    await Promise.all(
      // For each categorie
      command_categories.map(async (command_category) => {
        // Retrieve all the commands
        const command_files = await readdir(`${__dirname}/${command_category}`);

        // Add the category to the collection for the help command
        client.commands.categories.set(command_category, command_files.map(removeExtension));

        // Add the command
        return Promise.all(
          command_files.map(async (command_file) => {
            const command = (await import(`../commands/${command_category}/${command_file}`))
              .default;

            // Add it to the collection so the interaction will work
            command.data = command.data(client);
            client.commands.list.set(command.data.name, command);

            return command;
          }),
        );
      }),
    )
  ).flat(2);

  // Send guilds commands to Discord
  const scopedCommands = new Map<string, unknown[]>();

  // Add commands to guild where the bot is
  const allowedGuilds = client.guilds.cache;

  // Assign each commands to the guilds
  commands
    .filter((c) => c.scope().length > 0)
    .forEach((c) => {
      c.scope().forEach((guild: string) => {
        if (allowedGuilds.get(guild) !== undefined) {
          const guildCommands = scopedCommands.get(guild);
          if (guildCommands === undefined) {
            scopedCommands.set(guild, [c.data.toJSON()]);
          } else {
            guildCommands.push(c.data.toJSON());
            scopedCommands.set(guild, guildCommands);
          }
        }
      });
    });

  scopedCommands.forEach(
    async (command, guild) =>
      await rest.put(Routes.applicationGuildCommands(client.user!.id, guild), {
        body: command,
      }),
  );

  // Send global commands to Discord
  const globalCommands = commands.filter((c) => c.scope().length === 0);
  return await rest.put(Routes.applicationCommands(client.user!.id), {
    body: globalCommands.map((c) => c.data.toJSON()),
  });
};
