import { QueueRepeatMode, useQueue } from "discord-player";
import { ChatInputCommandInteraction, Client } from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    const loc_default = client.locales.get(client.config.default_lang);
    if (!loc_default) {
      return;
    }

    return (
      new SlashCommandBuilder()
        .setName(filename.toLowerCase())
        .setDescription(loc_default.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // Disable repeat
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub1_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub1_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub1_desc`)),
        )

        // Repeat current track
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub2_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub2_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub2_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub2_desc`)),
        )

        // Repeat queue
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub3_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub3_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub3_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub3_desc`)),
        )

        // Enable autoplay
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub4_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub4_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub4_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub4_desc`)),
        )
    );
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc_default = client.locales.get(client.config.default_lang);
    const filename = getFilename(__filename);

    const loc = getLocale(client, interaction.locale);
    const queue = useQueue();

    if (queue) {
      const subcommand = interaction.options.getSubcommand();
      switch (subcommand) {
        // Disable
        case loc_default?.get(`c_${filename}_sub1_name`)?.toLowerCase(): {
          queue.setRepeatMode(QueueRepeatMode.OFF);
          return interaction.reply(loc.get("c_repeat2") + ".");
        }

        // Queue Repeat
        case loc_default?.get(`c_${filename}_sub3_name`)?.toLowerCase(): {
          queue.setRepeatMode(QueueRepeatMode.QUEUE);
          return interaction.reply(`${loc.get("c_repeat3")} ${loc.get("c_repeat6")}.`);
        }

        // Autoplay
        case loc_default?.get(`c_${filename}_sub4_name`)?.toLowerCase(): {
          queue.setRepeatMode(QueueRepeatMode.AUTOPLAY);
          return interaction.reply(`${loc.get("c_repeat4")} ${loc.get("c_repeat6")}.`);
        }

        // Track repeat
        case loc_default?.get(`c_${filename}_sub2_name`)?.toLowerCase(): {
          queue.setRepeatMode(QueueRepeatMode.TRACK);
          return interaction.reply(
            `${loc.get("c_repeat5")} ${queue.history.currentTrack?.title} ${loc.get("c_repeat6")}.`,
          );
        }
      }
    }

    return await interaction.reply(`❌ | ${loc.get("c_repeat1")}`);
  },
};
