import { useQueue } from "discord-player";
import { ChatInputCommandInteraction, Client } from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    const loc_default = client.locales.get(client.config.default_lang);
    if (!loc_default) {
      return;
    }

    return (
      new SlashCommandBuilder()
        .setName(filename.toLowerCase())
        .setDescription(loc_default.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // Command option
        .addNumberOption((option) =>
          option
            .setName(loc_default.get(`c_${filename}_opt1_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_opt1_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_opt1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_opt1_desc`)),
        )
    );
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc_default = client.locales.get(client.config.default_lang);
    const filename = getFilename(__filename);

    const loc = getLocale(client, interaction.locale);
    const queue = useQueue();

    const id = interaction.options.getNumber(loc_default?.get(`c_${filename}_opt1_name`) as string);

    if (queue) {
      let msg;
      if (id) {
        queue.node.skipTo(id - 1);
        msg = loc.get("c_skip3") + " #" + id + "...";
      } else {
        queue.node.skip();
        msg = loc.get("c_skip1") + "...";
      }

      return await interaction.reply(msg);
    }

    return await interaction.reply(`❌ | ${loc.get("c_skip2")}`);
  },
};
