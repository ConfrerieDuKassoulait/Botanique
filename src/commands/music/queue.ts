import { useQueue } from "discord-player";
import {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  ChatInputCommandInteraction,
  Client,
  EmbedBuilder,
} from "discord.js";
import { v4 as uuidv4 } from "uuid";

import { SlashCommandBuilder } from "@discordjs/builders";

import { collect } from "../../buttons/loader";
import { embedListQueue } from "../../utils/commands/music";
import { musicQueueNext, musicQueuePrec } from "../../utils/constants";
import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    const loc_default = client.locales.get(client.config.default_lang);
    if (!loc_default) {
      return;
    }

    return (
      new SlashCommandBuilder()
        .setName(filename.toLowerCase())
        .setDescription(loc_default.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // Show the queue
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub1_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub1_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub1_desc`))

            // Specified Page
            .addNumberOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_sub1_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_sub1_opt1_desc`)!)
                .setNameLocalizations(
                  getLocalizations(client, `c_${filename}_sub1_opt1_name`, true),
                )
                .setDescriptionLocalizations(
                  getLocalizations(client, `c_${filename}_sub1_opt1_desc`),
                ),
            ),
        )

        // Shuffle Queue
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub2_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub2_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub2_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub2_desc`)),
        )

        // Remove <ID>
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub3_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub3_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub3_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub3_desc`))

            // Specified ID
            // TODO?: ID range -> as a string: 5-8 remove 5, 6, 7, 8
            // https://git.mylloon.fr/ConfrerieDuKassoulait/Botanique/issues/185
            .addNumberOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_sub3_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_sub3_opt1_desc`)!)
                .setNameLocalizations(
                  getLocalizations(client, `c_${filename}_sub3_opt1_name`, true),
                )
                .setDescriptionLocalizations(
                  getLocalizations(client, `c_${filename}_sub3_opt1_desc`),
                )
                .setRequired(true),
            ),
        )
    );
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc_default = client.locales.get(client.config.default_lang);
    const filename = getFilename(__filename);

    const loc = getLocale(client, interaction.locale);
    const queue = useQueue();

    const embed = new EmbedBuilder();

    if (queue) {
      const subcommand = interaction.options.getSubcommand();
      switch (subcommand) {
        // Show the queue
        case loc_default?.get(`c_${filename}_sub1_name`)?.toLowerCase(): {
          const page =
            interaction.options.getNumber(loc_default!.get(`c_${filename}_sub1_opt1_name`)!) ?? 1;

          embedListQueue(client, embed, queue, page, interaction.locale);

          const rows = [];
          const idPrec = musicQueuePrec + uuidv4();
          const idNext = musicQueueNext + uuidv4();
          rows.push(
            new ActionRowBuilder<ButtonBuilder>()
              .addComponents(
                new ButtonBuilder()
                  .setCustomId(idPrec)
                  .setLabel(loc.get(`c_${filename}8`))
                  .setStyle(ButtonStyle.Primary),
              )
              .addComponents(
                new ButtonBuilder()
                  .setCustomId(idNext)
                  .setLabel(loc.get(`c_${filename}9`))
                  .setStyle(ButtonStyle.Primary),
              ),
          );

          const callback = await interaction.reply({
            embeds: [embed],
            components: rows,
            withResponse: true,
          });

          // Buttons interactions
          collect(client, interaction, idPrec, callback.resource!.message!);
          collect(client, interaction, idNext, callback.resource!.message!);

          return;
        }

        // Shuffle Queue
        case loc_default?.get(`c_${filename}_sub2_name`)?.toLowerCase(): {
          queue.tracks.shuffle();

          embed.setDescription(loc.get("c_queue3"));
          break;
        }

        // Remove <ID>
        case loc_default?.get(`c_${filename}_sub3_name`)?.toLowerCase(): {
          const id = interaction.options.getNumber(
            loc_default!.get(`c_${filename}_sub3_opt1_name`)!,
          )!;

          const track = queue.removeTrack(id - 1);

          if (track) {
            embed.setDescription(
              `${loc.get("c_queue4")} #${id} \`${track.title}\` ${loc.get("c_queue5")}`,
            );
          } else {
            embed.setDescription(loc.get("c_queue6"));
          }

          break;
        }
      }
    } else {
      embed.setDescription(loc.get("c_queue2"));
    }

    return await interaction.reply({ embeds: [embed] });
  },
};
