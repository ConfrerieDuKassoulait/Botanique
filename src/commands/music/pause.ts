import { useQueue } from "discord-player";
import { ChatInputCommandInteraction, Client, EmbedBuilder } from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import { QueueStatus, toggleMusicPause } from "../../utils/commands/music";
import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    const loc_default = client.locales.get(client.config.default_lang);
    if (!loc_default) {
      return;
    }

    return new SlashCommandBuilder()
      .setName(filename.toLowerCase())
      .setDescription(loc_default.get(`c_${filename}_desc`)!)
      .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
      .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`));
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);
    const queue = useQueue();

    if (queue) {
      const embed = new EmbedBuilder();
      if (toggleMusicPause(queue) == QueueStatus.Play) {
        embed.setDescription(loc.get("c_pause1"));
        return await interaction.reply({ embeds: [embed] });
      } else {
        embed.setDescription(loc.get("c_pause2"));
        return await interaction.reply({ embeds: [embed] });
      }
    }

    return await interaction.reply(`❌ | ${loc.get("c_pause3")}`);
  },
};
