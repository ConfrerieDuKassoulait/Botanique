import { SearchResult, useMainPlayer, useQueue } from "discord-player";
import {
  AutocompleteInteraction,
  ChatInputCommandInteraction,
  Client,
  EmbedBuilder,
  MessageFlags,
} from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import {
  discord_limit_autocompletion_list_length,
  discord_limit_autocompletion_value_length,
} from "../../utils/constants";
import { getLocale, getLocalizations } from "../../utils/locales";
import { Metadata } from "../../utils/metadata";
import { getFilename } from "../../utils/misc";
import { timeToString } from "../../utils/time";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    const loc_default = client.locales.get(client.config.default_lang);
    if (!loc_default) {
      return;
    }

    return (
      new SlashCommandBuilder()
        .setName(filename.toLowerCase())
        .setDescription(loc_default.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // Normal
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub1_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub1_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub1_desc`))

            // Command option
            .addStringOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_opt1_desc`)!)
                .setNameLocalizations(getLocalizations(client, `c_${filename}_opt1_name`, true))
                .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_opt1_desc`))
                .setAutocomplete(true),
            ),
        )

        // Play now
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub2_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub2_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub2_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub2_desc`))

            // Command option
            .addStringOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_opt1_desc`)!)
                .setNameLocalizations(getLocalizations(client, `c_${filename}_opt1_name`, true))
                .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_opt1_desc`))
                .setAutocomplete(true),
            ),
        )
    );
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);
    const loc_default = client.locales.get(client.config.default_lang);
    const filename = getFilename(__filename);

    const member = client.guilds.cache
      .get(interaction.guildId!)
      ?.members.cache.get(interaction.member!.user.id);

    if (!member?.voice.channelId) {
      return await interaction.reply({
        content: `❌ | ${loc.get("c_play1")}`,
        flags: [MessageFlags.Ephemeral],
      });
    }

    if (
      interaction.guild?.members.me?.voice.channelId &&
      member.voice.channelId !== interaction.guild.members.me.voice.channelId
    ) {
      return await interaction.reply({
        content: `❌ | ${loc.get("c_play2")} ${interaction.guild.members.me.voice.channel}`,
        flags: [MessageFlags.Ephemeral],
      });
    }

    const query = interaction.options.getString(loc_default!.get(`c_${filename}_opt1_name`)!);

    const player = useMainPlayer();
    if (!query) {
      // Now playing

      const queue = useQueue();

      if (queue) {
        const track = queue.history.currentTrack;
        if (track) {
          const embed = new EmbedBuilder()
            .setDescription(
              `${queue.node.createProgressBar()}\n\n${loc.get("c_play8")} ${track.requestedBy}`,
            )
            .setTitle(track.title + " • " + track.author)
            .setURL(track.url)
            .setThumbnail(track.thumbnail)
            .setFooter({ text: loc.get("c_play7") })
            .setTimestamp();
          return await interaction.reply({ embeds: [embed] });
        }
      }

      const embed = new EmbedBuilder().setDescription(`❌ | ${loc.get("c_play6")}`);
      return await interaction.reply({ embeds: [embed] });
    }

    const queue = player.nodes.create(interaction.guild!, {
      volume: 50,
      defaultFFmpegFilters: ["silenceremove"],
      leaveOnEndCooldown: 15000,
      metadata: {
        interaction: interaction,
      } as Metadata,
    });

    // Verify vc connection
    try {
      if (!queue.connection) await queue.connect(member.voice.channel!);
    } catch {
      queue.delete();
      return await interaction.reply({
        content: `❌ | ${loc.get("c_play3")}`,
        flags: [MessageFlags.Ephemeral],
      });
    }

    await interaction.deferReply();
    const result = await player
      .search(query, {
        requestedBy: interaction.user,
      })
      .then((x) => x);

    if (result.isEmpty()) {
      const embed = new EmbedBuilder().setDescription(`❌ | \`${query}\` ${loc.get("c_play4")}.`);
      return await interaction.followUp({ embeds: [embed] });
    }

    let title;
    if (result.playlist) {
      queue.addTrack(result.playlist.tracks);
      title = result.playlist.title;
    } else {
      const track = result.tracks[0];

      if (
        interaction.options.getSubcommand() ===
        loc_default?.get(`c_${filename}_sub2_name`)?.toLowerCase()
      ) {
        queue.insertTrack(track, 0);
      } else {
        queue.addTrack(track);
      }

      title = track.title;
    }

    if (!queue.node.isPlaying()) {
      queue.node.play();
    }

    const positionEstimation = () => {
      const pos = queue.node.getTrackPosition(result.tracks[0]) + 1;

      if (pos === 0) {
        return loc.get("c_play_sub2_name");
      }

      const estimation = timeToString(
        [queue.currentTrack, ...queue.tracks.toArray()]
          .filter((t) => t !== null)
          .slice(0, pos)
          .reduce((total, t) => {
            if (total === 0) {
              return queue.dispatcher ? t.durationMS - queue.dispatcher.streamTime : t.durationMS;
            }
            return total + t.durationMS;
          }, 0),
      );
      return `${loc.get("c_play10")} ${pos} (${loc.get("c_play11")} ≈${estimation})`;
    };

    return await interaction.followUp({
      content: `⏱️ | \`${title}\` ${loc.get("c_play5")}, ${loc.get("c_play12")} ${positionEstimation()}.`,
    });
  },

  autocomplete: async (interaction: AutocompleteInteraction) => {
    const loc = getLocale(interaction.client, interaction.locale);
    const loc_default = interaction.client.locales.get(interaction.client.config.default_lang);
    const filename = getFilename(__filename);

    const player = useMainPlayer();
    const query = interaction.options.getString(loc_default!.get(`c_${filename}_opt1_name`)!, true);

    const limit_value_discord = discord_limit_autocompletion_value_length;
    const limit_element_discord = discord_limit_autocompletion_list_length;

    const query_discord = query.slice(0, limit_value_discord);

    if (query) {
      /* Since Discord wanna receive a response within 3 secs and results is async
       * and can take longer than that, exception of type 'Unknown interaction' (10062)
       * happens. */

      let timeoutId: NodeJS.Timeout;
      const delay = new Promise(function (_, reject) {
        timeoutId = setTimeout(function () {
          reject(new Error());
        }, 2800);
      });

      /* Create a race between a timeout and the search
       * At the end, Discord will always receive a response */
      const tracks = await Promise.race([
        delay,
        player.search(query, {
          requestedBy: interaction.user,
          searchEngine: "spotifySearch",
        }),
      ])
        .then((res) => {
          clearTimeout(timeoutId);
          return (res as SearchResult).tracks;
        })
        .catch(() => {
          return [];
        });

      // If tracks found
      if (tracks.length > 0) {
        const payload = tracks
          // Assure that URL is under the limit of Discord
          .filter((v) => v.url.length < limit_value_discord)
          // Slice the list to respect the limit of Discord
          .slice(0, limit_element_discord - 1)
          .map((t) => {
            let title = t.title;
            let author = t.author;
            let name = `${title} • ${author}`;

            // Slice returned data if needed to not exceed the length limit
            if (name.length > limit_value_discord) {
              const newTitle = title.substring(0, 40);
              if (title.length != newTitle.length) {
                title = `${newTitle}...`;
              }
              const newAuthor = author.substring(0, 40);
              if (author.length != newAuthor.length) {
                author = `${newAuthor}...`;
              }
              name = `${newTitle} • ${newAuthor}`;
            }

            return {
              name,
              value: t.url,
            };
          });

        payload.unshift({
          name: query_discord,
          value: query_discord,
        });

        // Returns a list of songs with their title and author
        return interaction.respond(payload);
      }
    }

    return interaction.respond([{ name: loc.get("c_play9"), value: query_discord }]);
  },
};
