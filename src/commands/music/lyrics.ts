import { useMainPlayer, useQueue } from "discord-player";
import { ChatInputCommandInteraction, Client, Message, MessageFlags } from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import { embedLyrics, findLyricsFromPlayer } from "../../utils/commands/music";
import { discord_limit_embed_per_message, discord_limit_message } from "../../utils/constants";
import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    const loc_default = client.locales.get(client.config.default_lang);
    if (!loc_default) {
      return;
    }

    return (
      new SlashCommandBuilder()
        .setName(filename.toLowerCase())
        .setDescription(loc_default.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // Normal
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub1_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub1_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub1_desc`))

            // Command option
            .addStringOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_opt1_desc`)!)
                .setNameLocalizations(getLocalizations(client, `c_${filename}_opt1_name`, true))
                .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_opt1_desc`)),
            ),
        )

        // Romanized
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub2_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub2_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub2_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub2_desc`))

            // Command option
            .addStringOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_opt1_desc`)!)
                .setNameLocalizations(getLocalizations(client, `c_${filename}_opt1_name`, true))
                .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_opt1_desc`)),
            ),
        )

        // Synced start
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub3_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub3_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub3_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub3_desc`)),
        )

        // Synced stop
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub4_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub4_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub4_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub4_desc`)),
        )
    );
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc_default = client.locales.get(client.config.default_lang);
    const filename = getFilename(__filename);

    const loc = getLocale(client, interaction.locale);

    let request = interaction.options.getString(loc_default!.get(`c_${filename}_opt1_name`)!);

    let data = null;
    await interaction.deferReply();

    const player = useMainPlayer();
    const queue = useQueue();
    if (request) {
      if (
        interaction.options.getSubcommand() ===
        loc_default?.get(`c_${filename}_sub2_name`)?.toLowerCase()
      ) {
        // Romanized
        request += " romanized";
      }

      try {
        data = await player.lyrics.search({ q: request });
      } catch {
        return await interaction.followUp(`❌ | ${loc.get("c_lyrics2")} \`${request}\``);
      }
    } else if (queue) {
      const track = queue.history.currentTrack;
      if (track) {
        try {
          request = track.title;
          data = await findLyricsFromPlayer(player, track);
        } catch {
          return await interaction.followUp(`❌ | ${loc.get("c_lyrics2")} \`${track.title}\``);
        }
      }
    }

    if (
      interaction.options.getSubcommand() ===
      loc_default?.get(`c_${filename}_sub3_name`)?.toLowerCase()
    ) {
      if (queue === null) {
        return await interaction.followUp(`❌ | ${loc.get("c_lyrics1")}`);
      }

      if (data === null || !data[0] || !data[0].syncedLyrics) {
        return await interaction.followUp(
          `❌ | ${loc.get("c_lyrics3")} \`${queue.currentTrack?.cleanTitle}\``,
        );
      }

      // Load lyrics
      if (queue.syncedLyricsMemory !== undefined) {
        return await interaction.followUp(loc.get("c_lyrics9"));
      }

      const syncedLyrics = queue.syncedLyrics(data[0]);
      queue.syncedLyricsMemory = syncedLyrics;

      let message: Message;
      syncedLyrics?.onChange(async (lyrics) => {
        if (interaction.channel?.isSendable()) {
          if (message) {
            const payload = message.cleanContent + "\n" + lyrics;
            if (payload.length < discord_limit_message) {
              message.edit(payload);
              return;
            }
          }
          message = await interaction.channel?.send(
            (message ? loc.get("c_lyrics6") + " " : "") +
              `${data[0].artistName} : **${data[0].trackName}**\n\n` +
              lyrics,
          );
        } else {
          await interaction.followUp(loc.get("c_lyrics5"));
        }
      });

      // Live update
      syncedLyrics.subscribe();

      syncedLyrics.onUnsubscribe(() => {
        queue.syncedLyricsMemory = undefined;
      });

      return await interaction.followUp({
        content: `🎤 | ${loc.get("c_lyrics4")}`,
        flags: [MessageFlags.Ephemeral],
      });
    }

    if (
      interaction.options.getSubcommand() ===
      loc_default?.get(`c_${filename}_sub4_name`)?.toLowerCase()
    ) {
      if (queue === null) {
        return await interaction.followUp(`❌ | ${loc.get("c_lyrics1")}`);
      }

      if (data === null || !data[0] || !data[0].syncedLyrics) {
        return await interaction.followUp(
          `❌ | ${loc.get("c_lyrics3")} \`${queue.currentTrack?.cleanTitle}\``,
        );
      }

      // Load lyrics
      if (queue.syncedLyricsMemory !== undefined && queue.syncedLyricsMemory.isSubscribed()) {
        queue.syncedLyricsMemory.unsubscribe();
        return await interaction.followUp(loc.get("c_lyrics7"));
      }

      return await interaction.followUp(loc.get("c_lyrics8"));
    }

    if (data) {
      const embeds = embedLyrics(data[0]);
      if (embeds.length === 0) {
        return await interaction.followUp(`❌ | ${loc.get("c_lyrics2")} \`${request}\``);
      }

      // TODO: Send multiple messages if there is too many embeds, instead of not sending the end
      return await interaction.followUp({
        embeds: embeds.slice(0, discord_limit_embed_per_message),
      });
    }

    return await interaction.followUp(`❌ | ${loc.get("c_lyrics1")}`);
  },
};
