import { useMainPlayer } from "discord-player";
import { ChatInputCommandInteraction, Client } from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    const loc_default = client.locales.get(client.config.default_lang);
    if (!loc_default) {
      return;
    }

    return new SlashCommandBuilder()
      .setName(filename.toLowerCase())
      .setDescription(loc_default.get(`c_${filename}_desc`)!)
      .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
      .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`));
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);

    const player = useMainPlayer();
    const queue = player.nodes.create(interaction.guild!, {
      metadata: {
        channel: interaction.channel,
      },
    });

    if (!(queue.connection || queue.node.isPlaying())) {
      return interaction.reply(`❌ | ${loc.get("c_stop1")}`);
    }

    queue.delete();

    interaction.reply(loc.get("c_stop2"));
  },
};
