import { ChatInputCommandInteraction, Client } from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    return new SlashCommandBuilder()
      .setName(filename.toLowerCase())
      .setDescription(client.locales.get(client.config.default_lang)!.get(`c_${filename}_desc`)!)
      .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
      .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`));
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);

    const sent = await interaction.reply({
      content: "Pinging...",
      withResponse: true,
    });

    const total = sent.resource!.message!.createdTimestamp - interaction.createdTimestamp;
    const ws = client.ws.ping == -1 ? loc?.get("c_ping3") : client.ws.ping + "ms";

    interaction.editReply(`${loc?.get("c_ping1")} ${total}ms\n${loc?.get("c_ping2")} ${ws}.`);
  },
};
