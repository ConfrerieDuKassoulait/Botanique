import "../../modules/string";

import {
  ApplicationCommandOptionType,
  ChatInputCommandInteraction,
  Client,
  Colors,
  EmbedBuilder,
  MessageFlags,
} from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import {
  goodDescription,
  goodName,
  NameNotLocalized,
  SubnameNotLocalized,
} from "../../utils/commands/help";
import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    return (
      new SlashCommandBuilder()
        .setName(filename.toLowerCase())
        .setDescription(client.locales.get(client.config.default_lang)!.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // Command option
        .addStringOption((option) =>
          option
            .setName(
              client.locales.get(client.config.default_lang)!.get(`c_${filename}_opt1_name`)!,
            )
            .setDescription(
              client.locales.get(client.config.default_lang)!.get(`c_${filename}_opt1_desc`)!,
            )
            .setNameLocalizations(getLocalizations(client, `c_${filename}_opt1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_opt1_desc`)),
        )
    );
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);
    const desired_command = interaction.options.getString(
      client.locales
        .get(client.config.default_lang)!
        .get(`c_${getFilename(__filename)}_opt1_name`)!,
    );

    // If a command isn't specified
    if (!desired_command) {
      const fields: {
        name: string;
        value: string;
      }[] = [];

      // Load all the command per categories
      client.commands.categories.forEach((commands_name, category) => {
        // Check if the command exist in the context (guild)
        commands_name = commands_name.filter((command) => {
          const scope = client.commands.list.get(command)?.scope();
          return scope!.length === 0 || scope?.find((v) => v === interaction.guildId) !== undefined;
        });

        // Add subcommands
        const all_commands: string[] = [];
        commands_name.forEach((command) => {
          const data = client.commands.list.get(command)?.data;
          const name = goodName(data!, interaction.locale);
          all_commands.push(name);

          data
            ?.toJSON()
            .options?.filter((option) => option.type === ApplicationCommandOptionType.Subcommand)
            .forEach((subcommand) =>
              all_commands.push(name + " " + goodName(subcommand, interaction.locale)),
            );
        });

        const commands = all_commands.reduce(
          (data, command_name) => data + `\`/${command_name}\`, `,
          "",
        );

        fields.push({
          name: category.capitalize() + ` (${all_commands.length})`,
          value: commands.slice(0, -2),
        });
      });

      // Sends a list of commands sorted into categories
      return interaction.reply({
        embeds: [
          new EmbedBuilder()
            .setColor(Colors.Blurple)
            .setTitle(loc.get("c_help1"))
            .setDescription(loc.get("c_help2"))
            .addFields(fields),
        ],
      });
    }

    const error = `${loc.get("c_help3")} \`${desired_command}\``;

    const [possible_command, possible_subcommand] = desired_command.split(" ");

    const command = NameNotLocalized(client, possible_command);
    if (!command) {
      return interaction.reply({ content: error, flags: [MessageFlags.Ephemeral] });
    }

    const scope = client.commands.list.get(command.name)?.scope();
    if (scope!.length > 0 && scope?.find((id) => id === interaction.guildId) === undefined) {
      // Command not available for the current guild
      return interaction.reply({ content: error, flags: [MessageFlags.Ephemeral] });
    }

    let subcommand = undefined;
    if (possible_subcommand) {
      subcommand = SubnameNotLocalized(command, possible_subcommand);
    } else {
      subcommand = null;
    }

    if (!command || subcommand === undefined) {
      // Sub/Command don't exist
      return interaction.reply({ content: error, flags: [MessageFlags.Ephemeral] });
    }

    // Loads the data according to the user's locals
    const requestedName =
      goodName(command, interaction.locale) +
      (subcommand !== null ? " " + goodName(subcommand, interaction.locale) : "");
    const requestedDesc = goodDescription(
      subcommand !== null ? subcommand : command,
      interaction.locale,
    );

    // Send information about the command
    return interaction.reply({
      embeds: [
        new EmbedBuilder()
          .setColor(Colors.Blurple)
          .setTitle("`/" + requestedName + "`")
          .setDescription(requestedDesc),
      ],
    });
  },
};
