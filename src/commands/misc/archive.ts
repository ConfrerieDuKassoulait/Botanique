import "../../modules/string";

import {
  ChannelType,
  Client,
  Colors,
  CommandInteraction,
  EmbedBuilder,
  MessageFlags,
  NonThreadGuildBasedChannel,
} from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => ["807244911350906920"],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    return (
      new SlashCommandBuilder()
        .setName(filename.toLowerCase())
        .setDescription(client.locales.get(client.config.default_lang)!.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // Command option
        .addStringOption((option) =>
          option
            .setName(
              client.locales.get(client.config.default_lang)!.get(`c_${filename}_opt1_name`)!,
            )
            .setDescription(
              client.locales.get(client.config.default_lang)!.get(`c_${filename}_opt1_desc`)!,
            )
            .setNameLocalizations(getLocalizations(client, `c_${filename}_opt1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_opt1_desc`)),
        )
    );
  },

  interaction: async (interaction: CommandInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);
    const desiredCat = interaction.options.get(
      client.locales
        .get(client.config.default_lang)!
        .get(`c_${getFilename(__filename)}_opt1_name`)!,
    )?.value as string;

    // If a category isn't specified
    if (!desiredCat) {
      // Sends a list of commands sorted into categories
      return interaction.reply({
        embeds: [
          new EmbedBuilder()
            .setColor(Colors.Blurple)
            .setTitle(loc.get("c_archive1"))
            .setDescription(loc.get("c_archive2")),
        ],
      });
    }

    // If a category is specified
    const cleanCat = ["L1", "L2", "L3", "M1", "M2"];
    const channel = cleanCat.includes(desiredCat);
    if (!channel) {
      // Category doesn't exist or is not included
      return interaction.reply({
        content: `${loc.get("c_archive3")} \`${desiredCat}\``,
        flags: [MessageFlags.Ephemeral],
      });
    }

    const allChannel = interaction.guild?.channels.fetch();
    allChannel?.then(async (channelGuild) => {
      // Retrieve category to archive
      const catToArchive = channelGuild
        .filter((chan) => chan?.type === ChannelType.GuildCategory)
        .filter((chan) => chan?.name === desiredCat);

      // Create/Retrieve the archive category
      const catArchivedName = "archive - " + desiredCat;
      const catArchivedMap = channelGuild
        .filter((chan) => chan?.type === ChannelType.GuildCategory)
        .filter((chan) => chan?.name === catArchivedName);

      let catArchived: NonThreadGuildBasedChannel | null | undefined;
      if (catArchivedMap.size > 0) {
        catArchived = catArchivedMap.at(0);
      } else {
        catArchived = await interaction.guild?.channels.create({
          name: catArchivedName,
          type: ChannelType.GuildCategory,
        });
      }

      const allChannelDesired = channelGuild
        .filter((chan) => chan?.type === 0)
        .filter((chan) => chan?.parentId === catToArchive.map((cat) => cat?.id)[0]);

      // If no channels in the source category
      if (allChannelDesired.size === 0) {
        return interaction.reply({
          embeds: [
            new EmbedBuilder()
              .setColor(Colors.Blurple)
              .setTitle(loc.get("c_archive6"))
              .setDescription(loc.get("c_archive7")),
          ],
        });
      }

      // Move channels to the archived categoryx
      allChannelDesired.forEach((elem) => elem?.setParent(catArchived?.id as string));

      return interaction.reply({
        embeds: [
          new EmbedBuilder()
            .setColor(Colors.Blurple)
            .setTitle(
              loc.get("c_archive4") +
                " `" +
                catToArchive.map((cat) => cat?.name) +
                "` " +
                loc.get("c_archive5") +
                " `" +
                catArchivedName +
                "`",
            )
            .setDescription(
              allChannelDesired
                .map((cgD) => cgD?.name)
                .toString()
                .replaceAll(",", "\n"),
            ),
        ],
      });
    });
  },
};
