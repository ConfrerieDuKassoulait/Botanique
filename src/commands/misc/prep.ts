import "../../modules/string";

import {
  ChannelType,
  Client,
  Colors,
  CommandInteraction,
  EmbedBuilder,
  MessageFlags,
} from "discord.js";

import { SlashCommandBuilder } from "@discordjs/builders";

import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => ["807244911350906920"],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    return (
      new SlashCommandBuilder()
        .setName(filename.toLowerCase())
        .setDescription(client.locales.get(client.config.default_lang)!.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // Command option
        .addStringOption((option) =>
          option
            .setName(
              client.locales.get(client.config.default_lang)!.get(`c_${filename}_opt1_name`)!,
            )
            .setDescription(
              client.locales.get(client.config.default_lang)!.get(`c_${filename}_opt1_desc`)!,
            )
            .setNameLocalizations(getLocalizations(client, `c_${filename}_opt1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_opt1_desc`)),
        )
    );
  },

  interaction: async (interaction: CommandInteraction, client: Client) => {
    const loc = getLocale(client, interaction.locale);
    const desired_cat = interaction.options.get(
      client.locales
        .get(client.config.default_lang)!
        .get(`c_${getFilename(__filename)}_opt1_name`)!,
    )?.value as string;

    // If a category isn't specified
    if (!desired_cat) {
      // Sends a list of commands sorted into categories
      return interaction.reply({
        embeds: [
          new EmbedBuilder()
            .setColor(Colors.Blurple)
            .setTitle(loc.get("c_prep1"))
            .setDescription(loc.get("c_prep2")),
        ],
      });
    }

    // If a category is specified
    const allowedCategories = ["L1", "L2", "L3", "M1", "M2"];
    const channel = allowedCategories.includes(desired_cat);
    if (!channel) {
      // Category doesn't exist or is not allowed
      return interaction.reply({
        content: `${loc.get("c_prep3")} \`${desired_cat}\``,
        flags: [MessageFlags.Ephemeral],
      });
    }

    // Send information about the command
    const allChannel = interaction.guild?.channels.fetch();
    allChannel?.then((channel_guild) => {
      const cat_to_prep = channel_guild
        .filter((chan) => chan?.type === ChannelType.GuildCategory)
        .filter((chan) => chan?.name === desired_cat);
      const cat_to_prep_id = cat_to_prep.map((cat) => cat?.id);
      const cat_to_prep_name = cat_to_prep.map((cat) => cat?.name);

      // console.log(cat_to_prep);
      const all_channel_desired = channel_guild
        .filter((chan) => chan?.type === 0)
        .filter((chan) => chan?.parentId === cat_to_prep_id[0]);
      const all_channel_desired_name = all_channel_desired.map((c_d) => c_d?.name);

      let desc = "";

      const general = "général";
      if (all_channel_desired_name.filter((cdn) => cdn === general).length === 0) {
        interaction.guild?.channels.create({
          name: general,
          type: 0,
          parent: cat_to_prep_id[0],
        });
        desc = general + loc.get("c_prep5") + "\n";
      }

      const info = "informations";
      if (all_channel_desired_name.filter((cdn) => cdn === info).length === 0) {
        interaction.guild?.channels.create({
          name: info,
          type: 0,
          parent: cat_to_prep_id[0],
        });

        desc += "`" + info + "` " + loc.get("c_prep5") + "\n";
      }

      if (desc === "") {
        desc = loc.get("c_prep6");
      }

      return interaction.reply({
        embeds: [
          new EmbedBuilder()
            .setColor(Colors.Blurple)
            .setTitle(loc.get("c_prep4") + cat_to_prep_name)
            .setDescription(desc),
        ],
      });
    });
  },
};
