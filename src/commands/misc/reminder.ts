import {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  ChatInputCommandInteraction,
  Client,
  MessageFlags,
  ModalBuilder,
  TextInputBuilder,
  TextInputStyle,
} from "discord.js";
import { v4 as uuidv4 } from "uuid";

import { ModalActionRowComponentBuilder, SlashCommandBuilder } from "@discordjs/builders";

import { collect } from "../../buttons/loader";
import {
  checkOwnershipReminder,
  deleteReminder,
  embedListReminders,
  getReminderInfo,
  newReminder,
} from "../../utils/commands/reminder";
import { reminderListNext, reminderListPrec } from "../../utils/constants";
import { getLocale, getLocalizations } from "../../utils/locales";
import { getFilename } from "../../utils/misc";

export default {
  scope: () => [],

  data: (client: Client) => {
    const filename = getFilename(__filename);
    const loc_default = client.locales.get(client.config.default_lang);
    if (!loc_default) {
      return;
    }

    return (
      new SlashCommandBuilder()
        // Command
        .setName(filename.toLowerCase())
        .setDescription(loc_default.get(`c_${filename}_desc`)!)
        .setNameLocalizations(getLocalizations(client, `c_${filename}_name`, true))
        .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_desc`))

        // New reminder
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub1_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub1_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub1_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub1_desc`))

            // Specified Time
            .addStringOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_sub1_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_sub1_opt1_desc`)!)
                .setNameLocalizations(
                  getLocalizations(client, `c_${filename}_sub1_opt1_name`, true),
                )
                .setDescriptionLocalizations(
                  getLocalizations(client, `c_${filename}_sub1_opt1_desc`),
                ),
            )

            // Specified message (not required)
            .addStringOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_sub1_opt2_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_sub1_opt2_desc`)!)
                .setNameLocalizations(
                  getLocalizations(client, `c_${filename}_sub1_opt2_name`, true),
                )
                .setDescriptionLocalizations(
                  getLocalizations(client, `c_${filename}_sub1_opt2_desc`),
                ),
            ),
        )

        // List reminders
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub2_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub2_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub2_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub2_desc`))

            // User
            .addUserOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_sub2_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_sub2_opt1_desc`)!)
                .setNameLocalizations(
                  getLocalizations(client, `c_${filename}_sub2_opt1_name`, true),
                )
                .setDescriptionLocalizations(
                  getLocalizations(client, `c_${filename}_sub2_opt1_desc`),
                ),
            )

            // Page
            .addIntegerOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_sub2_opt2_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_sub2_opt2_desc`)!)
                .setNameLocalizations(
                  getLocalizations(client, `c_${filename}_sub2_opt2_name`, true),
                )
                .setDescriptionLocalizations(
                  getLocalizations(client, `c_${filename}_sub2_opt2_desc`),
                ),
            ),
        )

        // Delete a reminder
        .addSubcommand((subcommand) =>
          subcommand
            .setName(loc_default.get(`c_${filename}_sub3_name`)!.toLowerCase())
            .setDescription(loc_default.get(`c_${filename}_sub3_desc`)!)
            .setNameLocalizations(getLocalizations(client, `c_${filename}_sub3_name`, true))
            .setDescriptionLocalizations(getLocalizations(client, `c_${filename}_sub3_desc`))

            // ID
            .addIntegerOption((option) =>
              option
                .setName(loc_default.get(`c_${filename}_sub3_opt1_name`)!.toLowerCase())
                .setDescription(loc_default.get(`c_${filename}_sub3_opt1_desc`)!)
                .setNameLocalizations(
                  getLocalizations(client, `c_${filename}_sub3_opt1_name`, true),
                )
                .setDescriptionLocalizations(
                  getLocalizations(client, `c_${filename}_sub3_opt1_desc`),
                )
                .setRequired(true),
            ),
        )
    );
  },

  interaction: async (interaction: ChatInputCommandInteraction, client: Client) => {
    const loc_default = client.locales.get(client.config.default_lang);
    const filename = getFilename(__filename);
    const loc = getLocale(client, interaction.locale);

    const subcommand = interaction.options.getSubcommand();
    switch (subcommand) {
      // New reminder
      case loc_default?.get(`c_${filename}_sub1_name`)?.toLowerCase(): {
        // If time is already renseigned
        const time = interaction.options.getString(
          loc_default!.get(`c_${filename}_sub1_opt1_name`)!,
        );
        if (time != null) {
          // Use the cli because we already have enough data
          return newReminder(client, time, {
            locale: interaction.locale,
            message: interaction.options.getString(
              loc_default!.get(`c_${filename}_sub1_opt2_name`)!,
            ),
            createdAt: interaction.createdAt.getTime(),
            channelId: interaction.channelId,
            userId: interaction.user.id,
            guildId: interaction.guildId,
          })
            .then((msg) =>
              interaction.reply({
                content: msg as string,
                flags: [MessageFlags.Ephemeral],
              }),
            )
            .catch((err) => {
              interaction.reply({
                content: err,
                flags: [MessageFlags.Ephemeral],
              });
            });
        } else {
          // Show modal to user to get at least the time
          const modal = new ModalBuilder()
            .setCustomId("reminderGUI")
            .setTitle(loc.get(`c_${filename}_name`).capitalize());

          const timeGUI = new TextInputBuilder()
            .setCustomId("reminderGUI-time")
            .setLabel(loc.get(`c_${filename}_sub1_opt1_name`).capitalize())
            .setStyle(TextInputStyle.Short)
            .setPlaceholder("1h")
            .setRequired(true);

          const messageGUI = new TextInputBuilder()
            .setCustomId("reminderGUI-message")
            .setLabel(loc.get(`c_${filename}_sub1_opt2_name`).capitalize())
            .setStyle(TextInputStyle.Paragraph)
            .setPlaceholder(loc.get(`c_${filename}_sub1_opt2_desc`))
            .setRequired(false);

          modal.addComponents(
            new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(timeGUI),
            new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(messageGUI),
          );

          return interaction.showModal(modal);
        }
      }
      // List reminders
      case loc_default?.get(`c_${filename}_sub2_name`)?.toLowerCase(): {
        // Which user to show
        let user = interaction.options.getUser(loc_default!.get(`c_${filename}_sub2_opt1_name`)!);
        if (user === null) {
          user = interaction.user;
        }

        const page =
          interaction.options.getInteger(loc_default!.get(`c_${filename}_sub2_opt2_name`)!) ?? 1;
        const list = await embedListReminders(
          client,
          user,
          interaction.guildId,
          page,
          interaction.locale,
        );

        const idPrec = reminderListPrec + uuidv4();
        const idNext = reminderListNext + uuidv4();
        const row = new ActionRowBuilder<ButtonBuilder>()
          .addComponents(
            new ButtonBuilder()
              .setCustomId(idPrec)
              .setLabel(loc.get(`c_${filename}12`))
              .setStyle(ButtonStyle.Primary),
          )
          .addComponents(
            new ButtonBuilder()
              .setCustomId(idNext)
              .setLabel(loc.get(`c_${filename}13`))
              .setStyle(ButtonStyle.Primary),
          );

        const callback = await interaction.reply({
          flags: [MessageFlags.Ephemeral],
          embeds: [list],
          components: [row],
          withResponse: true,
        });

        // Buttons interactions
        collect(client, interaction, idPrec, callback.resource!.message!);
        collect(client, interaction, idNext, callback.resource!.message!);

        return;
      }
      // Delete a reminder
      case loc_default?.get(`c_${filename}_sub3_name`)?.toLowerCase(): {
        const id = interaction.options.getInteger(
          loc_default!.get(`c_${filename}_sub3_opt1_name`)!,
        );
        if (id === null) {
          return interaction.reply({
            content: loc.get(`c_${filename}2`),
            flags: [MessageFlags.Ephemeral],
          });
        }

        // Check if the ID exists and belongs to the user
        if (
          await checkOwnershipReminder(client, id, interaction.user.id, interaction.guildId ?? "0")
        ) {
          return interaction.reply({
            content: loc.get(`c_${filename}3`),
            flags: [MessageFlags.Ephemeral],
          });
        }

        // Stop timeout
        const reminderInfo = await getReminderInfo(client, id);
        clearTimeout(reminderInfo.timeout_id);

        // Delete from database
        return deleteReminder(client, reminderInfo.creation_date, reminderInfo.user_id).then(() =>
          interaction.reply({
            content: `Reminder **#${id}** supprimé !`,
            flags: [MessageFlags.Ephemeral],
          }),
        );
      }
      default: {
        console.error(`${__filename}: unknown subcommand (${subcommand})`);
        break;
      }
    }
  },
};
