import { Client, MessageFlags, ModalSubmitInteraction } from "discord.js";

import { newReminder } from "../../utils/commands/reminder";
import { getFilename } from "../../utils/misc";

export default {
  data: {
    name: getFilename(__filename),
  },
  interaction: async (interaction: ModalSubmitInteraction, client: Client) => {
    const message = interaction.fields.fields.get("reminderGUI-message")?.value;

    return newReminder(client, interaction.fields.fields.get("reminderGUI-time")!.value, {
      locale: interaction.locale,
      message: message ? (message.length > 0 ? message : null) : null,
      createdAt: interaction.createdAt.getTime(),
      channelId: interaction.channelId,
      userId: interaction.user.id,
      guildId: interaction.guildId,
    }).then((msg) =>
      interaction.reply({
        content: msg as string,
        flags: [MessageFlags.Ephemeral],
      }),
    );
  },
};
