import { Client } from "discord.js";
import { readdir } from "fs/promises";

import { removeExtension } from "../utils/misc";

export default async (client: Client) => {
  // Dossier des modals
  const modals_categories = (await readdir(__dirname)).filter(
    (element) => !element.endsWith(".js") && !element.endsWith(".ts"),
  );

  await Promise.all(
    // For each categorie
    modals_categories.map(async (modals_category) => {
      // Retrieve all the commands
      const modal_files = await readdir(`${__dirname}/${modals_category}`);

      // Add the category to the collection for the help command
      client.modals.categories.set(modals_category, modal_files.map(removeExtension));

      // Add the modal
      return Promise.all(
        modal_files.map(async (modal_file) => {
          const modal = (await import(`../modals/${modals_category}/${modal_file}`)).default;

          // Add it to the collection so the interaction will work
          client.modals.list.set(modal.data.name, modal);
          return modal.data;
        }),
      );
    }),
  );
};
