SELECT
	EXISTS (
		SELECT
			1
		FROM
			reminder
		WHERE
			id = ?
			AND user_id = ?
			AND (
				guild_id = ?
				OR guild_id = 0
			)
	)
