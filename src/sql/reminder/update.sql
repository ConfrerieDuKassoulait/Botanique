UPDATE reminder
SET
	data = ?,
	expiration_date = ?,
	option_id = ?,
	channel_id = ?,
	creation_date = ?,
	user_id = ?,
	guild_id = ?,
	locale = ?,
	timeout_id = ?
WHERE
	ID = ?
