SELECT
	data,
	creation_date,
	expiration_date,
	id
FROM
	reminder
WHERE
	user_id = ?
	AND (
		guild_id = ?
		OR guild_id = 0
	)
