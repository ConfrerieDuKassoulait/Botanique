CREATE TABLE IF NOT EXISTS
	reminder (
		id INTEGER PRIMARY KEY,
		data TEXT,
		expiration_date TEXT,
		option_id INTEGER,
		channel_id TEXT,
		creation_date TEXT,
		user_id TEXT,
		guild_id TEXT,
		locale TEXT,
		timeout_id TEXT
	);
