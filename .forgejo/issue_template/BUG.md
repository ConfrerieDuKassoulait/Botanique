---
name: "🐞 Rapport d'un bug"
about: "Signal un problème rencontré"
ref: "main"
labels:
  - bug
  - "help wanted"
---

Bot version: v`X.Y.Z`

Problem :
