Checklist:

- [ ] Suivre les indications de `CONTRIBUTING.md`
<!-- - [ ] Augmente de 1 le numéro de version (soit `x.y.z`, augmentez `z` si
bugfix, `y` si ajout de fonctionnalité et `x` si grande nouveautée) -->
- [ ] Référence aux tickets (par exemple `Closes #xyz`)
