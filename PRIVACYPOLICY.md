# Privacy Policy for Botanique

## Introduction

Botanique ("we," "our," or "us") is a free Discord bot designed to enhance user experience on
Discord servers. This Privacy Policy outlines how we collect, use, and protect information when
you interact with Botanique.

## Information Collection and Use

Botanique processes user-generated content strictly to fulfill its functionalities. Some interactions
may require temporarily storing data for a limited time to ensure proper operation. Any stored
data is automatically deleted once it is no longer needed. We do not retain personal information
beyond what is necessary to provide the service.

## Data Security

We take appropriate measures to protect any temporarily stored data and ensure it is not accessed,
shared, or misused. Botanique only processes data within Discord’s infrastructure and does not
transmit it to third parties.

## Changes to This Privacy Policy

We may update this Privacy Policy from time to time. Any changes will be posted on this document.
We encourage you to review this policy periodically.

## Contact Us

If you have any questions or concerns about this Privacy Policy or Botanique's data practices, please
[open an issue on this repository](https://git.mylloon.fr/ConfrerieDuKassoulait/Botanique/issues/new/choose).
