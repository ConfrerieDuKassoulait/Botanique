# syntax=docker/dockerfile:1

FROM node:22-alpine

ENV DOCKERIZED=1
RUN mkdir /config && \
    chown node:node /config && \
    apk add dumb-init ffmpeg

WORKDIR /app
COPY --chown=node:node . .

ENV NODE_ENV=production
RUN npm ci && \
    npm run compile && \
    npm uninstall typescript && \
    rm -r src/ tsconfig.json && \
    npm cache clean --force

CMD ["dumb-init", "node", "./dist/index.js"]
