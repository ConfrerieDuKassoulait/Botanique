# 🌱 Botanique [![status-badge](https://git.mylloon.fr/ConfrerieDuKassoulait/Botanique/badges/workflows/publish.yml/badge.svg)](https://git.mylloon.fr/ConfrerieDuKassoulait/Botanique/actions?workflow=publish.yml)

[**Ajoute le bot à un serveur**](https://discord.com/api/oauth2/authorize?client_id=965598852407230494&permissions=8&scope=bot%20applications.commands)

## Lancer le bot

### Avec docker-compose (recommandé)

```yaml
services:
  botanique:
    image: git.mylloon.fr/confreriedukassoulait/botanique:latest
    container_name: Botanique
    environment:
      - TOKEN_DISCORD=ton-token-va-ici
    volumes:
      - /here/your/path:/config
    restart: unless-stopped
```

### En local

- Assurez-vous d'avoir [FFmpeg](https://ffmpeg.org/download.html) d'installé sur le système.
- Cloner le dépôt
- Spécifier un fichier `.env` en suivant [l'exemple](config/example.env)
- Installer les dépendances du bot

  ```bash
  npm install
  ```

- Lancer le bot

  ```bash
  npm run main
  ```

## Variables d'environnements

|      Nom      |    Description    | Par défaut |                                                                                     Commentaire                                                                                      |
| :-----------: | :---------------: | :--------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| TOKEN_DISCORD |   Token Discord   |   Aucune   |
| DEFAULT_LANG  | Langue par défaut |    `fr`    | Expérimental, si la langue par défaut n'est pas complète (c.-à-d. 100%), le bot pourrait ne pas fonctionner correctement.<br>Liste des traductions disponibles [ici](./src/locales/) |

## Volumes

|  Chemin   |                                 Description                                 |
| :-------: | :-------------------------------------------------------------------------: |
| `/config` | Dossier de configuration, par exemple, c'est ici que la base de donnée est. |

## Contribuer

Toute contribution est la bienvenue !

Pour commencer, lis le [fichier de contribution](./CONTRIBUTING.md).

## Licence

Voir le [fichier LICENCE](./LICENSE).

## Crédits

[Photo de profil](https://picrew.me/image_maker/1497656)
