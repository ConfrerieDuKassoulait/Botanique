import "../../src/modules/string";

import assert from "node:assert/strict";
import { describe, it } from "node:test";

describe("Capitalize", () => {
  {
    const name = "test";
    it(name, () => {
      assert.strictEqual(name.capitalize(), "Test");
    });
  }
  {
    const name = "MACHIN";
    it(name, () => {
      assert.strictEqual(name.capitalize(), "MACHIN");
    });
  }
  {
    const name = "tRUC";
    it(name, () => {
      assert.strictEqual(name.capitalize(), "TRUC");
    });
  }
  {
    const name = "Super";
    it(name, () => {
      assert.strictEqual(name.capitalize(), "Super");
    });
  }
  {
    it("Empty string", () => {
      assert.strictEqual("".capitalize(), "");
    });
  }
});
