import assert from "node:assert/strict";
import { describe, it } from "node:test";

import {
  nextTimeUnit,
  showDate,
  strToSeconds,
  timeDeltaToString,
  TimeSecond,
} from "../../src/utils/time";

describe("Date with correct timezone", () => {
  const map = new Map([["u_time_at", "@"]]);
  const date = new Date(1727434767686);
  {
    const name = "fr";
    it(name, () => {
      assert.strictEqual(showDate(name, map, date), "27/09/2024 @ 12:59:27");
    });
  }
  {
    const name = "en-US";
    it(name, () => {
      assert.strictEqual(showDate(name, map, date), "9/27/24, @ 1:59:27");
    });
  }
  {
    const name = "unknown";
    // Depends on the system
    // The important is that the date is in the correct timezone (UTC)
    it(name, () => {
      assert.ok(
        ["27/09/2024 @ 10:59:27", "9/27/24, @ 10:59:27"].includes(showDate(name, map, date)),
      );
    });
  }
  {
    const name = "zh-CN";
    it(name, () => {
      assert.strictEqual(showDate(name, map, date), "2024/9/27 @ 18:59:27");
    });
  }
});

describe("String time to seconds", () => {
  {
    const name = "10m30";
    it(name, () => {
      assert.strictEqual(strToSeconds(name), 630);
    });
  }
  {
    const name = "12h30";
    it(name, () => {
      assert.strictEqual(strToSeconds(name), 45000);
    });
  }
  {
    const name = "12s30";
    it(name, () => {
      assert.strictEqual(strToSeconds(name), 42);
    });
  }
  {
    const name = "1w30h20";
    it(name, () => {
      assert.strictEqual(strToSeconds(name), 714000);
    });
  }
});

describe("Next time unit", () => {
  {
    const name = TimeSecond.Minute;
    it(name.toString(), () => {
      assert.strictEqual(nextTimeUnit(name), TimeSecond.Second);
    });
  }
  {
    const name = TimeSecond.Hour;
    it(name.toString(), () => {
      assert.strictEqual(nextTimeUnit(name), TimeSecond.Minute);
    });
  }
  {
    const name = TimeSecond.Second;
    it(name.toString(), () => {
      assert.strictEqual(nextTimeUnit(name), TimeSecond.Second);
    });
  }
  {
    const name = TimeSecond.Year;
    it(name.toString(), () => {
      assert.strictEqual(nextTimeUnit(name), TimeSecond.Week);
    });
  }
});

describe("Relative time", () => {
  // Thoses its are based on time, we have 10s of acceptance.
  {
    const name = Date.now() + (10 * TimeSecond.Minute + 30) * 1000;
    it(name.toString(), () => {
      assert.match(timeDeltaToString(name), /10m 30s|10m 2\ds/);
    });
  }
  {
    const name = Date.now() + (12 * TimeSecond.Hour + 30 * TimeSecond.Minute) * 1000;
    it(name.toString(), () => {
      assert.match(timeDeltaToString(name), /12h 30m|12h 29m 5\ds/);
    });
  }
  {
    const name = Date.now() + (TimeSecond.Week + TimeSecond.Day + 6 * TimeSecond.Hour) * 1000;
    it(name.toString(), () => {
      assert.match(timeDeltaToString(name), /1w 1d 6h|1w 1d 5h 59m 5\ds/);
    });
  }
  {
    const name = Date.now();
    it(name.toString(), () => {
      assert.match(timeDeltaToString(name), /\ds/);
    });
  }
});
