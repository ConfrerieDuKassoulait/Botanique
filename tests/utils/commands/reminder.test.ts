import assert from "node:assert/strict";
import { describe, it } from "node:test";

import { OptionReminder, splitTime } from "../../../src/utils/commands/reminder";

describe("Time splitter", () => {
  {
    it("Empty String", () => {
      assert.deepStrictEqual(splitTime(""), { option: OptionReminder.Nothing, time: "" });
    });
  }
  {
    const name = "2m@p";
    it(name, () => {
      assert.deepStrictEqual(splitTime(name), { option: OptionReminder.DirectMessage, time: "2m" });
    });
  }
  {
    const name = "41@";
    it(name, () => {
      assert.deepStrictEqual(splitTime(name), { option: OptionReminder.Mention, time: "41" });
    });
  }
  {
    const name = "0P";
    it(name, () => {
      assert.deepStrictEqual(splitTime(name), { option: OptionReminder.DirectMessage, time: "0" });
    });
  }
});
