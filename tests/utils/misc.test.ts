import assert from "node:assert/strict";
import { describe, it } from "node:test";

import {
  cleanCodeBlock,
  emojiPng,
  isImage,
  removeExtension,
  splitFilenameExtensions,
} from "../../src/utils/misc";

describe("Filename splitter", () => {
  {
    const name = "test.js";
    it(name, () => {
      assert.deepStrictEqual(splitFilenameExtensions(name), { file: "test", ext: "js" });
    });
  }
  {
    const name = ".env";
    it(name, () => {
      assert.deepStrictEqual(splitFilenameExtensions(name), { file: ".env", ext: undefined });
    });
  }
  {
    const name = ".env.test";
    it(name, () => {
      assert.deepStrictEqual(splitFilenameExtensions(name), { file: ".env", ext: "test" });
    });
  }
  {
    const name = "file.test.js";
    it(name, () => {
      assert.deepStrictEqual(splitFilenameExtensions(name), { file: "file.test", ext: "js" });
    });
  }
});

describe("Extension remover", () => {
  {
    const name = "test.js";
    it(name, () => {
      assert.strictEqual(removeExtension(name), "test");
    });
  }
  {
    const name = ".env";
    it(name, () => {
      assert.strictEqual(removeExtension(name), ".env");
    });
  }
  {
    const name = ".env.test";
    it(name, () => {
      assert.strictEqual(removeExtension(name), ".env");
    });
  }
  {
    const name = "file.test.js";
    it(name, () => {
      assert.strictEqual(removeExtension(name), "file.test");
    });
  }
});

describe("Image checker", () => {
  {
    const name = "image.Png";
    it(name, () => {
      assert.strictEqual(isImage(name), true);
    });
  }
  {
    const name = "image.jpeg";
    it(name, () => {
      assert.strictEqual(isImage(name), true);
    });
  }
  {
    const name = "image.wav";
    it(name, () => {
      assert.strictEqual(isImage(name), false);
    });
  }
  {
    const name = "image.jpg";
    it(name, () => {
      assert.strictEqual(isImage(name), true);
    });
  }
  {
    const name = "image.webP";
    it(name, () => {
      assert.strictEqual(isImage(name), true);
    });
  }
  {
    const name = "image.GIF";
    it(name, () => {
      assert.strictEqual(isImage(name), true);
    });
  }
});

describe("Code block cleaner", () => {
  {
    const name = "salut";
    it(name, () => {
      assert.strictEqual(cleanCodeBlock(name), "`salut`");
    });
  }
  {
    const name = "<@158260864623968257> ça va ?";
    it(name, () => {
      assert.strictEqual(cleanCodeBlock(name), "<@158260864623968257>` ça va ?`");
    });
  }
  {
    const name = "t'as vu la vidéo ? https://youtu.be/dQw4w9WgXcQ";
    it(name, () => {
      assert.strictEqual(cleanCodeBlock(name), "`t'as vu la vidéo ? `https://youtu.be/dQw4w9WgXcQ");
    });
  }
  {
    const name = "t'as vu la vidéo ? https://youtu.be/dQw4w9WgXcQ elle est cool en vrai tqt";
    it(name, () => {
      assert.strictEqual(
        cleanCodeBlock(name),
        "`t'as vu la vidéo ? `https://youtu.be/dQw4w9WgXcQ` elle est cool en vrai tqt`",
      );
    });
  }
});

describe("Emoji to link", () => {
  {
    const name = "☺️";
    it(name, () => {
      assert.strictEqual(
        emojiPng(name),
        "https://cdn.jsdelivr.net/gh/twitter/twemoji/assets/72x72/263a.png",
      );
    });
  }
  {
    const name = "🍕";
    it(name, () => {
      assert.strictEqual(
        emojiPng(name),
        "https://cdn.jsdelivr.net/gh/twitter/twemoji/assets/72x72/1f355.png",
      );
    });
  }
});
