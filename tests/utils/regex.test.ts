import assert from "node:assert/strict";
import { describe, it } from "node:test";

import { RegexC, RegExpFlags } from "../../src/utils/regex";

describe("Regex flags", () => {
  it("One parameter", () => {
    const regex = RegexC("", RegExpFlags.Global);
    assert.strictEqual(regex.global, true);
  });

  it("All parameters", () => {
    const regex = RegexC(
      "",
      RegExpFlags.Global |
        RegExpFlags.MultiLine |
        RegExpFlags.Insensitive |
        RegExpFlags.Sticky |
        RegExpFlags.Unicode |
        RegExpFlags.SingleLine,
    );
    assert.strictEqual(regex.global, true);
    assert.strictEqual(regex.multiline, true);
    assert.strictEqual(regex.ignoreCase, true);
    assert.strictEqual(regex.sticky, true);
    assert.strictEqual(regex.unicode, true);
    assert.strictEqual(regex.dotAll, true);
  });
});
