# Terms of Service for Botanique

1. Acceptance of Terms

By inviting and using Botanique ("the Bot") on your Discord server, you ("the User") agree to
comply with and be bound by these Terms of Service. If you do not agree with these terms,
please refrain from using the Bot.

2. Description of Service

Botanique is a free Discord bot designed to enhance user experience on Discord servers.
The Bot operates within the Discord platform and utilizes Discord's API to provide its functionalities.

3. User Responsibilities

Compliance with Discord's Terms: Users must adhere to Discord's Terms of Service while using the Bot.

Prohibited Activities: Users are prohibited from:

- Using the Bot for any unlawful or malicious activities.
- Exploiting the Bot in any unauthorized manner.

4. Limitation of Liability

To the maximum extent permitted by law, Botanique and its developers shall not be liable for
any indirect, incidental, special, consequential, or punitive damages, or any loss of profits
or revenues, whether incurred directly or indirectly, resulting from:

- Your access to or use of or inability to access or use the Bot.
- Any unauthorized access, use, or alteration of your transmissions or content.

5. Disclaimer of Warranties

The Bot is provided on an "as is" basis without warranties of any kind,
either express or implied. Botanique disclaims any and all warranties and
conditions of merchantability, fitness for a particular purpose, and non-infringement.

6. Termination

We reserve the right to terminate or suspend access to the Bot at our sole discretion,
without prior notice or liability, for any reason whatsoever, including but not limited
to a breach of these Terms.

7. Changes to Terms

We may update these Terms of Service from time to time. Any changes will be posted on this document.
Continued use of the Bot after any such changes constitutes your acceptance of the new Terms.

8. Contact Information

If you have any questions or concerns about these Terms of Service, please
[open an issue on this repository](https://git.mylloon.fr/ConfrerieDuKassoulait/Botanique/issues/new/choose).
